# flutter_base_app

### Issue Found
- PagerSegmentedWidget not working
- Navigator [bindBuilder] not working




NOTES:

### Working with 'ProviderWidget' and 'Controller'

1.  First page in the app is 'LandingPage'.

2.  Created 'LandingController' on 'LandingPage' using 'ProviderWidget'.

3.  If relaunch app by clear stack and navigate to 'LandingPage', 'LandingController' will get destroyed and removed.

4.  If want ensure 'LandingController' will not get destroyed, when create 'LandingController' using 'ProviderWidget'
    must assign 'true' to 'permanent'.

5.  At the same time to make 'LandingController' as a fresh state, you can call Controllers.reset before start navigate.

### ProviderWidget and Controllers

1.  Every controller created by 'ProviderWidget', if didn't assign 'permanent' parameter, will follow widget's lifecycle,
    it destroyed when widget's dispose.

2.  Controller created before 'runApp' using Controllers.put or Controllers.putLazy, will not effect by widget's lifecycle,
    developer can delete or reset it when needed using Controllers.delete and Controllers.reset