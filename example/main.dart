import 'package:flutter/cupertino.dart';
import 'package:flutter_base_app/app/application.dart';
import 'package:flutter_base_app/app/resources/base_config.dart';
import 'package:flutter_base_app/app/root_app.dart';
import 'package:flutter_base_app/app/toolkit.dart';

import 'empty_page.dart';
import 'launcher_page.dart';

void main() {
  Application.instance.setConfig(BaseConfig());

  Toolkit.init();

  runApp(
    RootApp(
      onLaunch: () {
        Toolkit.navigator.pushPage(builder: () => LauncherPage(), clearStack: true);
      },
    ),
  );
}
