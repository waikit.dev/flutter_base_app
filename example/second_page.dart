import 'package:flutter/material.dart';
import 'package:flutter_base_app/ui/widget/appbar_widget.dart';
import 'package:flutter_base_app/ui/widget/single_page_widget.dart';

class SecondPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SinglePageWidget(
        child: Container(),
        appBar: AppbarWidget(
          title: 'Second Page',
        ));
  }
}
