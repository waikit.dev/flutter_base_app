import 'package:flutter/material.dart';
import 'package:flutter_base_app/app/application.dart';
import 'package:flutter_base_app/app/toolkit.dart';
import 'package:flutter_base_app/helpers/navigator.dart';
import 'package:flutter_base_app/ui/builder/general_view_builder.dart';
import 'package:flutter_base_app/ui/dialog/single_button_dialog.dart';
import 'package:flutter_base_app/ui/widget/button_widget.dart';
import 'package:flutter_base_app/ui/widget/password_textfield_widget.dart';
import 'package:flutter_base_app/ui/widget/text_widget.dart';
import 'package:flutter_base_app/helpers/dialog.dart';

import 'second_page.dart';

class LauncherPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          TextWidget(
            text: 'Hello World',
          ),
          PasswordTextFieldWidget(controller: TextEditingController(),),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Expanded(
                child: ButtonWidget(
                  height: 100.0,
                  child: Text('dsafdsfdsfds'),
                  onTap: () {
                    Toolkit.loader.showLoading();
                  },
                ),
              ),
              ButtonWidget(
                isWrap: true,
                height: 90.0,
                child: Text('dsafdsfdsfds'),
                onTap: () {
                  Toolkit.dialog.showBottomSheet(
                    backgroundColor: R.color.colorTransparent,
                    child: Container(
                      color: Colors.white,
                      child: Row(
                        children: [
                          Container(
                            width: 100,
                            height: 100,
                            color: R.color.colorPrimaryDark,
                          ),
                        ],
                      ),
                    ),
                  );
                },
              ),
            ],
          ),
        ],
      ),
    );
  }
}
