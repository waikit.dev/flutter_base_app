mixin Constants {
  static const String tel = 'tel';
  static const String mailto = 'mailto';
  static const String ios = 'iOS';
  static const String apple = 'Apple';
  static const String android = 'Android';
  static const String packages_flutter_base_app = 'packages/flutter_base_app/';
}