import 'package:flutter_base_app/app/toolkit.dart';
import 'package:super_qr_reader/super_qr_reader.dart';

mixin Routers {
  static Future launchQrScanner({required Function(String?) onScanned}) async {
    String data = await Toolkit.navigator.pushPage(builder: () => ScanView(hasImagePicker: false));
    onScanned.call(data);
  }
}
