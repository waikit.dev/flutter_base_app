import 'package:flutter_base_app/helpers/calendar.dart';
import 'package:flutter_base_app/helpers/colors.dart';
import 'package:flutter_base_app/helpers/device.dart';
import 'package:flutter_base_app/helpers/dialog.dart';
import 'package:flutter_base_app/helpers/encrypt.dart';
import 'package:flutter_base_app/helpers/https.dart';
import 'package:flutter_base_app/helpers/image.dart';
import 'package:flutter_base_app/helpers/launcher.dart';
import 'package:flutter_base_app/helpers/loader.dart';
import 'package:flutter_base_app/helpers/logger.dart';
import 'package:flutter_base_app/helpers/navigator.dart';
import 'package:flutter_base_app/helpers/permissions.dart';
import 'package:flutter_base_app/helpers/platform.dart';
import 'package:flutter_base_app/helpers/scanner.dart';
import 'package:flutter_base_app/helpers/screen.dart';
import 'package:flutter_base_app/helpers/secure_storage.dart';
import 'package:flutter_base_app/helpers/storage.dart';
import 'package:flutter_base_app/helpers/strings.dart';
import 'package:flutter_base_app/helpers/toast.dart';

/// 工具箱集合，在程序启动时必须手动初始化，配备的【init】方法为默认初始化方法，会根据不同的平台来初始化不同的工具类，
/// 开发者也可以使用各自的【set】方法来做定制的初始化
///
mixin Toolkit {
  static late Device _device;
  static Device get device => _device;
  static void setDevice(Device device) => _device = device;

  static late Colors _colors;
  static Colors get colors => _colors;
  static void setColors(Colors colors) => _colors = colors;

  static late Encrypt _encrypt;
  static Encrypt get encrypt => _encrypt;
  static void setEncrypt(Encrypt encrypt) => _encrypt = encrypt;

  static late Image _image;
  static Image get image => _image;
  static void setImage(Image image) => _image = image;

  static late Dialog _dialog;
  static Dialog get dialog => _dialog;
  static void setDialog(Dialog dialog) => _dialog = dialog;

  static late Navigator _navigator;
  static Navigator get navigator => _navigator;
  static void setNavigator(Navigator navigator) => _navigator = navigator;

  static late Screen _screen;
  static Screen get screen => _screen;
  static void setScreen(Screen screen) => _screen = screen;

  static late Strings _strings;
  static Strings get strings => _strings;
  static void setStrings(Strings strings) => _strings = strings;

  static late Toast _toast;
  static Toast get toast => _toast;
  static void setToast(Toast toast) => _toast = toast;

  static late Launcher _launcher;
  static Launcher get launcher => _launcher;
  static void setLauncher(Launcher launcher) => _launcher = launcher;

  static late Logger _logger;
  static Logger get logger => _logger;
  static void setLogger(Logger logger) => _logger = logger;

  static late Storage _storage;
  static Storage get storage => _storage;
  static void setStorage(Storage storage) => _storage = storage;

  static late SecureStorage _secureStorage;
  static SecureStorage get secureStorage => _secureStorage;
  static void setSecureStorage(SecureStorage secureStorage) => _secureStorage = secureStorage;

  static late Platform _platform;
  static Platform get platform => _platform;
  static void setPlatform(Platform platform) => _platform = platform;

  static late Https _https;
  static Https get https => _https;
  static void setHttps(Https https) => _https = https;

  static late Calendar _calendar;
  static Calendar get calendar => _calendar;
  static void setCalendar(Calendar calendar) => _calendar = calendar;

  static late Loader _loader;
  static Loader get loader => _loader;
  static void setLoader(Loader loader) => _loader = loader;

  static late Permissions _permissions;
  static Permissions get permissions => _permissions;
  static void setPermissions(Permissions permissions) => _permissions = permissions;

  static late Scanner _scanner;
  static Scanner get scanner => _scanner;
  static void setScanner(Scanner scanner) => _scanner = scanner;

  static void init() {
    createPlatform();
    createDevice();
    createColor();
    createEncrypt();
    createImage();
    createDialog();
    createNavigator();
    createScreen();
    createStrings();
    createToast();
    createLauncher();
    createLogger();
    createStorage();
    createSecureStorage();
    createHttps();
    createCalendar();
    createLoader();
    createPermissions();
    createScanner();
  }

  static void createLoader() {
    setLoader(Loader());
  }

  static void createDevice() {
    if (platform.isAndroid) {
      setDevice(AndroidDevice());
    } else if (platform.isIOS) {
      setDevice(IOSDevice());
    } else if (platform.isWeb) {
      setDevice(WebDevice());
    }
  }

  static void createColor() {
    setColors(Colors());
  }

  static void createEncrypt() {
    setEncrypt(Encrypt());
  }

  static void createImage() {
    setImage(Image());
  }

  static void createDialog() {
    setDialog(Dialog());
  }

  static void createNavigator() {
    setNavigator(Navigator());
  }

  static void createScreen() {
    setScreen(Screen());
  }

  static void createStrings() {
    setStrings(Strings());
  }

  static void createToast() {
    setToast(Toast());
  }

  static void createLauncher() {
    setLauncher(Launcher());
  }

  static void createLogger() {
    setLogger(Logger());
  }

  static void createStorage() {
    setStorage(Storage());
  }

  static void createSecureStorage() {
    if (platform.isWeb) {
      setSecureStorage(WebSecureStorage());
    } else {
      setSecureStorage(SecureStorage());
    }
  }

  static void createPlatform() {
    setPlatform(Platform());
  }

  static void createHttps() {
    setHttps(Https());
  }

  static void createCalendar() {
    setCalendar(Calendar());
  }

  static void createPermissions() {
    setPermissions(Permissions());
  }

  static void createScanner() {
    setScanner(Scanner());
  }
}
