import 'dart:ui';

class BaseColor {
  BaseColor();

  Color colorAccent = Color(0xff2f29c7);

  Color colorPrimary = Color(0xfff43ba6);

  Color colorPrimaryDark = Color(0xffcc0e74);

  Color colorTitle = Color(0xffffffff);

  Color colorText = Color(0xff707070);

  Color colorHint = Color(0xffc6c6c6);

  Color colorDivider = Color(0xfff4f4f4);

  Color colorTransparent = Color(0xff);

  Color colorBlack = Color(0xff000000);

  Color colorWhite = Color(0xffffffff);

  Color colorDialogBg = Color(0xffffffff);

  Color colorScaffoldBg = Color(0xffffffff);

  Color colorActive = Color(0xff2f29c7);

  Color colorInactive = Color(0xffc6c6c6);

  Color colorLink = Color(0xff4687c9);

  Color colorAttention = Color(0xffff0808);

  Color colorTextFieldBorder = Color(0xffc6c6c6);

  Color colorCursor = Color(0xff2f29c7);

  Color colorDisabled = Color(0xffc6c6c6);
}
