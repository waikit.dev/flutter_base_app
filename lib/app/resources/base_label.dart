import 'package:get/get.dart';

extension Localized on String {
  String get localized => this.tr;
}

class BaseLabel {
  BaseLabel();

  String get unknown => 'unknown'.tr;

  String get phone_number_is_not_valid => 'phone_number_is_not_valid'.tr;

  String get email_is_not_valid => 'email_is_not_valid'.tr;

  String get success => 'success'.tr;

  String get warning => 'warning'.tr;

  String get error => 'error'.tr;

  String get ok => 'ok'.tr;

  String get yes => 'yes'.tr;

  String get no => 'no'.tr;

  String get seconds_ago => 'seconds_ago'.tr;

  String get minutes_ago => 'minutes_ago'.tr;

  String get hours_ago => 'hours_ago'.tr;

  String get days_ago => 'days_ago'.tr;

  String get weeks_ago => 'weeks_ago'.tr;

  String get choose_from_gallery => 'choose_from_gallery'.tr;

  String get take_from_camera => 'take_from_camera'.tr;

  String get close => 'close'.tr;

  String get click_here_to_retry => 'click_here_to_retry'.tr;

  String get unauthorised => 'unauthorised'.tr;

  String get click_here_to_login_again => 'click_here_to_login_again'.tr;

  String get no_data => 'no_data'.tr;

  String get share_text_is_not_valid => 'share_text_is_not_valid'.tr;

  String get data_parse_error => 'data_parse_error'.tr;

  String get scan_qr_from => 'scan_qr_from'.tr;

  String get camera => 'camera'.tr;

  String get photo => 'photo'.tr;

  String get something_went_wrong => 'something_went_wrong'.tr;
}
