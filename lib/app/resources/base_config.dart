class BaseConfig {
  BaseConfig._();

  BaseConfig({
    this.appName,
    this.appPublicKey,
    this.appPrivateKey,
    this.appSecretKey,
  });

  String? appName;

  String? appPublicKey;

  String? appPrivateKey;

  String? appSecretKey;

  factory BaseConfig.create({
    String? appName,
    String? baseUrl,
    String? appPublicKey,
    String? appPrivateKey,
    String? appSecretKey,
  }) {
    BaseConfig config = BaseConfig._();
    config.appName = appName;
    config.appPublicKey = appPublicKey;
    config.appPrivateKey = appPrivateKey;
    config.appSecretKey = appSecretKey;
    return config;
  }

  BaseConfig withConfig({
    String? appName,
    String? appPublicKey,
    String? appPrivateKey,
    String? appSecretKey,
  }) {
    this.appName = appName;
    this.appPublicKey = appPublicKey;
    this.appPrivateKey = appPrivateKey;
    this.appSecretKey = appSecretKey;
    return this;
  }
}
