import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_base_app/app/application.dart';
import 'package:flutter_base_app/app/resources/base_config.dart';
import 'package:flutter_base_app/app/resources/base_font.dart';
import 'package:flutter_base_app/app/controllers.dart';
import 'package:flutter_base_app/app/controller/app_controller.dart';
import 'package:flutter_base_app/app/controller/language_controller.dart';
import 'package:flutter_base_app/app/resources/base_image.dart';
import 'package:flutter_base_app/app/resources/base_label.dart';
import 'package:flutter_base_app/app/root_app.dart';
import 'package:flutter_base_app/app/toolkit.dart';
import 'package:flutter_base_app/helpers/https.dart';
import 'package:flutter_base_app/app/resources/base_color.dart';
import 'package:get/get.dart';

abstract class AppStart {
  AppStart(BaseConfig config) {
    Application.instance.setConfig(config);
  }

  void run() async {
    WidgetsFlutterBinding.ensureInitialized();

    /// 初始化程序资源
    Application.instance.setFont(font);
    Application.instance.setFontFamily(fontFamily);
    Application.instance.setTextTheme(textTheme);
    Application.instance.setDarkColor(darkColor);
    Application.instance.setLightColor(lightColor);
    Application.instance.setBrightnessDark(isDarkMode);
    Application.instance.setImage(image);
    Application.instance.setLabel(label);

    /// 设置工具箱
    Toolkit.init();
    Toolkit.logger.enable(isEnableLog);
    Toolkit.loader.setupLoadWidget(loaderWidget);
    Toolkit.secureStorage.setup(
      privateKey: R.config.appPrivateKey,
      publicKey: R.config.appPublicKey,
      secretKey: R.config.appSecretKey,
    );

    if (isHttpOverride) {
      HttpOverrides.global = new MyHttpOverrides();
    }

    /// 设置全局【controller】
    Controllers.put<AppController>(appController);
    Controllers.put<LanguageController>(languageController..init());

    await configureMiscellaneousTask();

    runApp(RootApp(
      homePage: homePage,
      onLaunch: onLaunch,
      initialRoutes: initialRoutes,
      pageList: pageList,
    ));

    await configureOrientation();
    await configureStatusBar();
  }

  Future configureStatusBar() async {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.dark.copyWith(
      statusBarColor: R.color.colorTransparent,
    ));
  }

  Future configureOrientation() async {
    await SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp, DeviceOrientation.portraitDown]);
  }

  Future configureMiscellaneousTask();

  bool get isEnableLog;

  bool get isHttpOverride;

  Widget get loaderWidget;

  AppController get appController;

  LanguageController get languageController;

  Widget get homePage;

  Function? get onLaunch;

  String get initialRoutes;

  List<GetPage> get pageList;

  bool get isDarkMode;

  String get fontFamily;

  BaseFont get font;

  BaseColor get lightColor;

  BaseColor get darkColor;

  BaseLabel get label;

  BaseImage get image;

  TextTheme? get textTheme;
}
