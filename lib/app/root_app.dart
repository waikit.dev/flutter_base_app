import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_base_app/app/application.dart';
import 'package:flutter_base_app/app/controllers.dart';
import 'package:flutter_base_app/app/controller/language_controller.dart';
import 'package:flutter_base_app/app/toolkit.dart';
import 'package:get/get.dart';
import 'package:oktoast/oktoast.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

class InitiatePage extends StatefulWidget {
  InitiatePage({this.onLaunch});

  final Function? onLaunch;

  @override
  _InitiatePageState createState() => _InitiatePageState();
}

class _InitiatePageState extends State<InitiatePage> {
  @override
  void initState() {
    super.initState();

    WidgetsBinding.instance?.addPostFrameCallback((timeStamp) {
      widget.onLaunch?.call();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      color: R.color.colorScaffoldBg,
    );
  }
}

class InitiatePage2 extends StatefulWidget {
  InitiatePage2({required this.child});

  final Widget child;

  @override
  _InitiatePageState2 createState() => _InitiatePageState2();
}

class _InitiatePageState2 extends State<InitiatePage2> {
  @override
  void initState() {
    super.initState();

    WidgetsBinding.instance?.addPostFrameCallback((timeStamp) {
      Toolkit.navigator.pushPage(builder: () => widget.child);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      color: R.color.colorScaffoldBg,
    );
  }
}

class RootApp extends StatelessWidget {
  const RootApp({
    required this.onLaunch,
    required this.homePage,
    required this.pageList,
    required this.initialRoutes,
  });

  final Widget homePage;

  final Function? onLaunch;

  final List<GetPage> pageList;

  final String initialRoutes;

  @override
  Widget build(BuildContext context) {
    return OKToast(
      child: GetMaterialApp(
        title: R.config.appName ?? 'Flutter App',
        debugShowCheckedModeBanner: false,
        theme: Application.instance.lightTheme,
        darkTheme: Application.instance.darkTheme,
        getPages: pageList,
        initialRoute: initialRoutes,
        defaultTransition: Transition.cupertino,
        translations: Controllers.get<LanguageController>()?.localization,
        locale: Controllers.get<LanguageController>()?.currentLocale,
        builder: (context, child) {
          return ScrollConfiguration(
            behavior: BouncingScrollBehaviour(),
            child: RefreshConfiguration(
              headerBuilder: () {
                return MaterialClassicHeader(
                  backgroundColor: R.color.colorDialogBg,
                  color: R.color.colorAccent,
                );
              },
              child: child ?? Container(),
            ),
          );
        },
      ),
    );
  }
}

class BouncingScrollBehaviour extends ScrollBehavior {
  BouncingScrollBehaviour();

  @override
  ScrollPhysics getScrollPhysics(BuildContext context) {
    return BouncingScrollPhysics();
  }
}

class _LauncherPage extends StatefulWidget {
  const _LauncherPage({
    required this.onLaunch,
  });

  final Function onLaunch;

  @override
  __LauncherPageState createState() => __LauncherPageState();
}

class __LauncherPageState extends State<_LauncherPage> {
  @override
  void initState() {
    super.initState();

    WidgetsBinding.instance?.addPostFrameCallback((timeStamp) async {
      widget.onLaunch.call();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        color: R.color.colorPrimary,
      ),
    );
  }
}
