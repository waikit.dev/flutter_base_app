import 'package:flutter_base_app/controller/controller.dart';
import 'package:get/get.dart';

mixin Controllers {

  /// 返回内存里存有的【controller】，如果找不到则返回空
  ///
  static T? get<T extends Controller>({String? tag}) {
    return isExists<T>(tag: tag) ? Get.find<T>(tag: tag) : null;
  }

  /// 当调用【put】时，【controller】会被马上初始化
  ///
  /// 当【permanent】返回【true】时，这个【controller】不能被删除，对【controller】采用【delete】和【reset】是无效的
  ///
  /// 当【permanent】返回【false】时，对【controller】采用【delete】后，后续如果没有继续对【controller】调用【put】和【putLazy】，
  /// 那么【controller】就不存在了。如果调用【reset】则会在成功删除【controller】后马上调用【put】或【putLazy】，让后续能继续使用。
  ///
  /// 如果需要【controller】不被【widget】的生命周期影响，最好使用【put】
  ///
  static void put<T extends Controller>(T object, {String? tag, bool permanent = false}) {
    Get.put(object, tag: tag, permanent: permanent);
  }

  /// 当【controller】被使用到时才初始化
  ///
  /// 当【permanent】返回【true】时，【builder】将会被保存
  ///
  /// 如果【controller】是在【widget】里被使用并且初始化，那它会跟随着【widget】的生命周期，当【widget】被【dispose】时，【controller】也会被删除，
  ///
  /// 如果不想【controller】被【widget】的生命周期影响，请使用【put】
  ///
  /// 调用【delete】的情况分析：
  /// 1。当【permanent】返回【true】，调用【delete】后，【controller】还能再次被使用但是状态不会被重设
  /// 2。当【permanent】返回【false】，调用【delete】后如果不再设置【put】或【putLazy】，后续则此【controller】不能再使用，会不存在于内存
  ///
  /// 调用【reset】的情况分析：
  /// 1。当【permanent】返回【true】，不管【widget】是不是第一页，调用【reset】再路由回相同【widget】并设置为第一页时，【controller】会存在但不会被重设
  /// 2。当【permanent】返回【false】并且【widget】为第一页时，调用【reset】再路由回相同【widget】并设置为第一页时，【controller】会被重设
  /// 3。当【permanent】返回【false】并且【widget】不是第一页时，调用【reset】再路由回相同【widget】并设置为第一页时，【controller】会不存在 （不知原因）
  ///
  /// 全局的【controller】，尽量不要使用这个方法
  ///
  static void putLazy<T extends Controller>(T object, {String? tag, bool permanent = false}) {
    Get.lazyPut(() => object, tag: tag, fenix: permanent);
  }

  /// 删除指定的【controller】，如果成功删除会调用【put】或【putLazy】来重设【controller】
  ///
  /// 具体【controller】能不能被重设，还需要根据调用【put】或【putLazy】时使用的【permanent】值
  ///
  static Future<bool> reset<T extends Controller>(T object, {String? tag, bool lazy = true}) async {
    bool success = await delete<T>(tag: tag);
    if (success) lazy ? putLazy(object, tag: tag) : put(object, tag: tag);
    return success;
  }

  /// 删除指定的【controller】，返回成功或失败
  ///
  static Future<bool> delete<T extends Controller>({String? tag}) async {
    bool success = await Get.delete<T>(tag: tag);
    return success;
  }

  /// 查看内存中是否存在指定的【controller】，存在则返回【true】
  ///
  static bool isExists<T extends Controller>({String? tag}) {
    return Get.isRegistered<T>(tag: tag);
  }
}
