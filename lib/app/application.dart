import 'package:flutter/material.dart';
import 'package:flutter_base_app/app/resources/base_label.dart';
import 'package:flutter_base_app/app/resources/base_config.dart';
import 'package:flutter_base_app/app/resources/base_color.dart';
import 'package:flutter_base_app/app/resources/base_font.dart';
import 'package:flutter_base_app/app/resources/base_image.dart';
import 'package:get/get.dart';

QuickAccess R = QuickAccess._();

class Application with _AppConfig, _AppTheme, _AppImage, _AppLabel {
  Application._();

  static Application _self = Application._();

  static Application get instance => _self;

  BuildContext? get context => Get.context;
}

mixin _AppConfig {
  BaseConfig _baseConfig = BaseConfig();

  void setConfig(BaseConfig config) => _baseConfig = config;
}

mixin _AppTheme {
  String? _fontFamily;

  TextTheme? _textTheme;

  BaseFont _baseFont = BaseFont();

  BaseColor _darkColor = BaseColor();

  BaseColor _lightColor = BaseColor();

  bool get isDarkMode => Get.isDarkMode;

  BaseColor get _baseColor => isDarkMode ? _darkColor : _lightColor;

  void setFont(BaseFont font) => _baseFont = font;

  void setFontFamily(String fontFamily) => _fontFamily = fontFamily;

  void setDarkColor(BaseColor color) => _darkColor = color;

  void setLightColor(BaseColor color) => _lightColor = color;

  void setTextTheme(TextTheme? textTheme) => _textTheme = textTheme;

  void toggleBrightness() => setBrightnessDark(!isDarkMode);

  void setBrightnessDark(bool isDark) {
    Get.changeTheme(isDark ? darkTheme : lightTheme);
    // Get.rootController.update();
  }

  ThemeData get darkTheme => _getThemeData(isDark: true);

  ThemeData get lightTheme => _getThemeData(isDark: false);

  ThemeData _getThemeData({required bool isDark}) => ThemeData(
        brightness: isDark ? Brightness.dark : Brightness.light,
        primaryColor: R.color.colorPrimary,
        accentColor: R.color.colorAccent,
        visualDensity: VisualDensity.adaptivePlatformDensity,
        scaffoldBackgroundColor: R.color.colorScaffoldBg,
        fontFamily: _fontFamily,
        textTheme: _textTheme ??
            TextTheme(
              /// 【Light】
              headline1: TextStyle(
                fontSize: 14.0,
                fontWeight: FontWeight.w300,
                color: R.color.colorText,
              ),

              /// 【Normal】
              headline2: TextStyle(
                fontSize: 14.0,
                fontWeight: FontWeight.w400,
                color: R.color.colorText,
              ),

              /// 【Medium】
              headline3: TextStyle(
                fontSize: 14.0,
                fontWeight: FontWeight.w500,
                color: R.color.colorText,
              ),

              /// 【SemiBold】
              headline4: TextStyle(
                fontSize: 14.0,
                fontWeight: FontWeight.w600,
                color: R.color.colorText,
              ),

              /// 【Bold】
              headline5: TextStyle(
                fontSize: 14.0,
                fontWeight: FontWeight.w700,
                color: R.color.colorText,
              ),

              /// 【ExtraBold】
              headline6: TextStyle(
                fontSize: 14.0,
                fontWeight: FontWeight.w900,
                color: R.color.colorText,
              ),
            ),
      );
}

mixin _AppImage {
  BaseImage _baseImage = BaseImage();

  void setImage(BaseImage image) => _baseImage = image;
}

mixin _AppLabel {
  BaseLabel _baseLabel = BaseLabel();

  void setLabel(BaseLabel label) => _baseLabel = label;
}

class QuickAccess {
  QuickAccess._();

  ThemeData get theme => Get.theme;

  TextTheme get textTheme => Get.theme.textTheme;

  /// 【Font】
  BaseFont get font => Application.instance._baseFont;

  /// 【Color】
  BaseColor get color => Application.instance._baseColor;

  /// 【Image】
  BaseImage get image => Application.instance._baseImage;

  /// 【Config】
  BaseConfig get config => Application.instance._baseConfig;

  /// 【Label】
  BaseLabel get label => Application.instance._baseLabel;
}
