import 'dart:ui';

import 'package:flutter/cupertino.dart';
import 'package:flutter_base_app/app/toolkit.dart';
import 'package:flutter_base_app/controller/state_list_controller.dart';
import 'package:get/get.dart';

abstract class LanguageController extends StateListController {
  LanguageController();

  _Localization _localization = _Localization();

  _Localization get localization => _localization;

  Locale? _currentLocale;

  Locale? get currentLocale => _currentLocale;

  String get currentLanguageCode => currentLocale?.languageCode ?? 'en';

  List<Locale> _localeList = [];

  List<Locale> get localeList => _localeList;

  Locale localeAtIndex(int index) => localeList.elementAt(index);

  int get availableLocaleCount => localeList.length;

  bool isLanguageSelectAtIndex(int index) => currentLocale == localeAtIndex(index);

  String languageCodeAtIndex(int index) => localeAtIndex(index).languageCode;

  String nameAtIndex(int index) => getLanguageNameByLanguageCode(languageCodeAtIndex(index));

  Locale getDefaultLocale();

  Future<Locale?> getCachedLocale();

  Future<Map<String, String>> getTranslationsList(String languageCode);

  void openLanguageSelectionList();

  Future init() async {
    Locale? deviceLocale = getDeviceLocale();
    Locale? cachedLocale = await getCachedLocale();

    await initData();

    if (isLocaleSupported(cachedLocale, localeList)) {
      _currentLocale = cachedLocale;
    } else if (isLocaleSupported(deviceLocale, localeList)) {
      _currentLocale = deviceLocale;
    }

    if (currentLocale == null) _currentLocale = getDefaultLocale();

    await changeLanguageByLocale(currentLocale);
  }

  bool isLocaleSupported(Locale? locale, List<Locale> localeList) {
    return isLanguageCodeSupported(locale?.languageCode, localeList);
  }

  bool isLanguageCodeSupported(String? languageCode, List<Locale> localeList) {
    List<String> codeList = localeList.map((e) => e.languageCode).toList();
    return codeList.contains(languageCode);
  }

  @override
  Future<List<Locale>> loadData({int page = 1}) async {
    _localeList = await getLocaleList();
    return _localeList;
  }

  Locale? getDeviceLocale() {
    String? _deviceLocale = Toolkit.device.deviceLocale?.languageCode;
    return _deviceLocale == null ? null : Locale(_deviceLocale);
  }

  Future<List<Locale>> getLocaleList() async {
    return [
      Locale('en'),
      Locale('zh'),
    ];
  }

  Future changeLanguageByLocale(Locale? locale) async {
    if (locale == null) return;

    Map<String, String> translations = await getTranslationsList(locale.languageCode);
    _currentLocale = locale;
    localization._addTranslations(locale, translations);
    Get.updateLocale(locale);
  }

  String getLanguageNameByLanguageCode(String languageCode) {
    String name = '';

    switch (languageCode) {
      case 'en':
        name = "English";
        break;
      case 'zh':
        name = "简体中文";
        break;
    }

    return name;
  }
}

class _Localization extends Translations {
  _Localization();

  Map<String, Map<String, String>> _translations = {
    'en': {
      'yes': 'Yes',
      'no': 'No',
      'warning': 'Warning',
      'ok': 'OK',
      'success': 'Success',
      'days_ago': 'days ago',
      'unauthorised': 'Unauthorised',
      'scan_qr_from': 'Scan QR From',
      'camera': 'Camera',
      'photo': 'Photo'
    },
    'zh': {
      'yes': '是',
      'no': '不是',
      'warning': '提醒',
      'ok': '好的',
      'success': '成功',
      'days_ago': '天前',
      'unauthorised': '未经授权',
      'scan_qr_from': '扫描QR码',
      'camera': '相机',
      'photo': '图片'
    },
  };

  @override
  Map<String, Map<String, String>> get keys => _translations;

  void _addTranslations(Locale locale, Map<String, String> translations) {
    if (translations.length == 0) return;
    if (!_translations.containsKey(locale.toLanguageTag())) {
      _translations[locale.toLanguageTag()] = {};
    }
    translations.forEach((key, value) {
      _translations[locale.toLanguageTag()]?[key] = value;
    });
  }
}
