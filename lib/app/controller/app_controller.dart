import 'package:flutter_base_app/controller/controller.dart';

class AppController extends Controller {
  AppController();

  Map<String, Controller> _queueControllers = {};

  void registerResetController(String name, Controller controller) => _queueControllers[name] = controller;

  void removeResetController(String name, Controller controller) => _queueControllers.remove(name);

  void relaunchApp({Function? onNavigate}) {
    _queueControllers.entries.forEach((element) {
      element.value.onReset();
    });
    onNavigate?.call();
  }
}
