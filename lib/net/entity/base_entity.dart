import 'package:flutter_base_app/app/application.dart';
import 'package:flutter_base_app/helpers/https.dart';

typedef Decoder<T> = T Function(Map<String, dynamic>);

abstract class NetworkResponseMixin {
  int? statusCode;

  String? dioMessage;

  bool get isSuccess => statusCode == HttpResponseCode.SUCCESS;

  bool get isBadRequest => statusCode == HttpResponseCode.BAD_REQUEST;

  bool get isUnauthorised => statusCode == HttpResponseCode.UNAUTHORISED;

  bool get is404NotFound => statusCode == HttpResponseCode.NOT_FOUND_404;

  bool get isMethodNotAllow => statusCode == HttpResponseCode.METHOD_NOT_ALLOW;

  bool get isInternalServerError => statusCode == HttpResponseCode.INTERNAL_SERVER_ERROR;

  bool get isParseDataError => statusCode == HttpResponseCode.PARSE_ERROR;

  bool get isUnknownError => statusCode == HttpResponseCode.UNKNOWN_ERROR;

  bool get isCancelRequest => statusCode == HttpResponseCode.CANCEL_REQUEST;

  bool get isConnectionTimeout => statusCode == HttpResponseCode.CONNECTION_TIMEOUT;

  void printResponseInfo() {
    print({
      'status_code': statusCode,
      'dio_message': dioMessage,
    });
  }
}

class BaseEntity<T> with NetworkResponseMixin {
  BaseEntity._();

  int? code;

  String? message;

  T? data;

  List<T>? list;

  factory BaseEntity.createWithNetworkResponse({
    int? statusCode,
    String? dioMessage,
    Map<String, dynamic>? json,
    Decoder<T>? decoder,
  }) {
    BaseEntity<T> baseResponse = BaseEntity<T>._();
    baseResponse.statusCode = statusCode;
    baseResponse.dioMessage = dioMessage;
    if (json != null) {
      try {
        baseResponse._fromJson(json, decoder);
      } catch (ex) {
        baseResponse.statusCode = HttpResponseCode.PARSE_ERROR;
        baseResponse.dioMessage = R.label.data_parse_error;
      }
    }
    return baseResponse;
  }

  BaseEntity<T> _fromJson(Map<String, dynamic> json, Decoder<T>? decoder) {
    code = json['rst'];
    message = json['msg'];

    var _json = json['data'];
    if (_json != null) {
      if (_json is List) {
        list = [];
        _json.forEach((element) {
          if (decoder != null) list?.add(decoder.call(element));
        });
      } else {
        data = decoder?.call(_json);
      }
    }

    return this;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['code'] = this.code;
    data['message'] = this.message;
    return data;
  }
}
