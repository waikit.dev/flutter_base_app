import 'package:flutter_base_app/app/toolkit.dart';
import 'package:flutter_base_app/helpers/https.dart';
import 'package:flutter_base_app/net/entity/base_entity.dart';

abstract class IServerRepository<T> {
  Future<BaseEntity<T>> getEntity<T>({
    required String baseUrl,
    required String path,
    Map<String, dynamic>? queryParameters,
    Map<String, dynamic>? headers,
    Decoder<T>? decoder,
  });

  Future<BaseEntity<T>> postEntity<T>({
    required String baseUrl,
    required String path,
    Map<String, dynamic>? data,
    Map<String, dynamic>? headers,
    Decoder<T>? decoder,
  });
}

class ServerRepository<T> extends IServerRepository<T> {
  ServerRepository._();

  static ServerRepository _self = ServerRepository._();

  static ServerRepository get instance => _self;

  @override
  Future<BaseEntity<T>> getEntity<T>({
    required String baseUrl,
    required String path,
    Map<String, dynamic>? queryParameters,
    Map<String, dynamic>? headers,
    Decoder<T>? decoder,
  }) async {
    DioResultModel model = await Toolkit.https.getRequest(
      baseUrl: baseUrl,
      path: path,
      queryParameters: queryParameters,
      headers: headers,
    );
    model = process(model);
    BaseEntity<T> entity = BaseEntity.createWithNetworkResponse(
      statusCode: model.code,
      dioMessage: model.message,
      json: model.data,
      decoder: decoder,
    );
    return entity;
  }

  @override
  Future<BaseEntity<T>> postEntity<T>({
    required String baseUrl,
    required String path,
    Map<String, dynamic>? data,
    Map<String, dynamic>? headers,
    Decoder<T>? decoder,
  }) async {
    DioResultModel model = await Toolkit.https.postRequest(
      baseUrl: baseUrl,
      path: path,
      data: data,
      headers: headers,
    );
    model = process(model);
    BaseEntity<T> entity = BaseEntity.createWithNetworkResponse(
      statusCode: model.code,
      dioMessage: model.message,
      json: model.data,
      decoder: decoder,
    );
    return entity;
  }

  DioResultModel process(DioResultModel model) {
    if (model.code == HttpResponseCode.SUCCESS) {
      if (model.data != null && model.data['rst'] == 0) {
        model.code = HttpResponseCode.BAD_REQUEST;
      }
    }

    return model;
  }
}
