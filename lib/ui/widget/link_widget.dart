import 'package:flutter/material.dart';
import 'package:flutter_base_app/app/application.dart';
import 'package:flutter_base_app/helpers/screen.dart';
import 'package:flutter_base_app/ui/widget/text_widget.dart';

class LinkWidget extends StatelessWidget {
  const LinkWidget({
    required this.text,
    this.onTap,
    this.padding,
    this.textStyle,
    this.textAlign,
  });

  final String text;

  final Function? onTap;

  final TextStyle? textStyle;

  final TextAlign? textAlign;

  final EdgeInsets? padding;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      highlightColor: R.color.colorTransparent,
      splashColor: R.color.colorTransparent,
      onTap: () {
        onTap?.call();
      },
      child: Padding(
        padding: padding ??
            EdgeInsets.symmetric(
              horizontal: 8.0.scale(),
              vertical: 0.0.scale(),
            ),
        child: TextWidget(
          text: text,
          textStyle: textStyle ?? R.textTheme.headline2?.copyWith(color: R.color.colorLink),
          textAlign: textAlign,
        ),
      ),
    );
  }
}
