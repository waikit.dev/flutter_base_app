import 'package:flutter/material.dart';
import 'package:flutter_base_app/app/application.dart';
import 'package:flutter_base_app/helpers/screen.dart';
import 'package:flutter_base_app/ui/widget/text_widget.dart';

class TextFieldWidget extends StatelessWidget {
  const TextFieldWidget({
    required this.controller,
    this.maxLines,
    this.label,
    this.hint,
    this.inputType,
    this.isRequired = false,
    this.isObscure = false,
    this.isEnable = true,
    this.isInteractive = true,
    this.isShowLabel = true,
    this.inputAction,
    this.prefixWidget,
    this.suffixWidget,
    this.labelTextStyle,
    this.hintTextStyle,
    this.inputTextStyle,
    this.contentPadding,
    this.disabledInputBorder,
    this.enabledInputBorder,
    this.focusedInputBorder,
  });

  final int? maxLines;

  final bool isObscure;

  final bool isRequired;

  final bool isEnable;

  final bool isInteractive;

  final bool isShowLabel;

  final String? label;

  final String? hint;

  final Widget? prefixWidget;

  final Widget? suffixWidget;

  final TextInputType? inputType;

  final TextInputAction? inputAction;

  final TextEditingController controller;

  final TextStyle? labelTextStyle;

  final TextStyle? hintTextStyle;

  final TextStyle? inputTextStyle;

  final EdgeInsets? contentPadding;

  final InputBorder? disabledInputBorder;

  final InputBorder? enabledInputBorder;

  final InputBorder? focusedInputBorder;

  TextStyle? get _labelTextStyle =>
      labelTextStyle ?? R.textTheme.headline2?.copyWith(color: R.color.colorText, fontSize: 14.0.scale());

  TextStyle? get _inputTextStyle =>
      inputTextStyle ?? R.textTheme.headline2?.copyWith(color: R.color.colorText, fontSize: 16.0.scale());

  TextStyle? get _hintTextStyle =>
      hintTextStyle ?? R.textTheme.headline2?.copyWith(color: R.color.colorHint, fontSize: 16.0.scale());

  EdgeInsets get _contentPadding =>
      contentPadding ?? EdgeInsets.fromLTRB(4.0.scale(), 0.0.scale(), 4.0.scale(), 0.0.scale());

  InputBorder get _disableInputBorder =>
      disabledInputBorder ??
      UnderlineInputBorder(borderSide: BorderSide(color: R.color.colorDisabled, width: 1.0.scale()));

  InputBorder get _enabledInputBorder =>
      enabledInputBorder ??
      UnderlineInputBorder(borderSide: BorderSide(color: R.color.colorTextFieldBorder, width: 1.0.scale()));

  InputBorder get _focusedInputBorder =>
      focusedInputBorder ??
      UnderlineInputBorder(borderSide: BorderSide(color: R.color.colorAccent, width: 1.0.scale()));

  TextInputAction get _inputAction => inputAction ?? TextInputAction.next;

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Visibility(
          visible: isShowLabel,
          child: Row(
            children: [
              TextWidget(
                text: label,
                textStyle: _labelTextStyle,
              ),
              Visibility(
                visible: isRequired,
                child: Row(
                  children: [
                    SizedBox(
                      width: 2.0.scale(),
                    ),
                    TextWidget(
                      text: "*",
                      textStyle: R.textTheme.headline2?.copyWith(color: R.color.colorAttention, fontSize: 12.0.scale()),
                    )
                  ],
                ),
              ),
            ],
          ),
        ),
        SizedBox(
          height: 8.0.scale(),
        ),
        TextField(
          enabled: isEnable,
          enableInteractiveSelection: isInteractive,
          textInputAction: _inputAction,
          controller: controller,
          maxLines: maxLines,
          cursorColor: R.color.colorAccent,
          obscureText: isObscure,
          autocorrect: false,
          style: _inputTextStyle,
          keyboardType: inputType,
          decoration: InputDecoration(
            suffixIcon: suffixWidget,
            suffixIconConstraints: BoxConstraints(minWidth: 0, minHeight: 0),
            prefixIcon: prefixWidget,
            prefixIconConstraints: BoxConstraints(minWidth: 0, minHeight: 0),
            hintText: hint ?? '',
            hintStyle: _hintTextStyle,
            isDense: true,
            contentPadding: _contentPadding,
            disabledBorder: _disableInputBorder,
            enabledBorder: _enabledInputBorder,
            focusedBorder: _focusedInputBorder,
          ),
        ),
      ],
    );
  }
}
