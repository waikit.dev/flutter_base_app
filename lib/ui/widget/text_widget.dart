import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter_base_app/app/application.dart';

class TextWidget extends StatelessWidget {
  const TextWidget({
    this.text,
    this.textStyle,
    this.isAutoText = true,
    this.textAlign,
    this.textSpans,
  });

  final bool isAutoText;

  final String? text;

  final TextAlign? textAlign;

  final TextStyle? textStyle;

  final List<TextSpan>? textSpans;

  TextStyle? get _textStyle => textStyle ?? R.textTheme.headline2?.copyWith(fontSize: 16.0);

  TextAlign get _textAlign => textAlign ?? TextAlign.start;

  @override
  Widget build(BuildContext context) {
    return isAutoText
        ? textSpans != null
            ? AutoSizeText.rich(
                TextSpan(
                  text: text,
                  style: _textStyle,
                  children: textSpans,
                ),
                textAlign: _textAlign,
              )
            : AutoSizeText(
                text,
                textAlign: _textAlign,
                style: _textStyle,
              )
        : Text(
            text ?? '',
            textAlign: _textAlign,
            style: _textStyle,
          );
  }
}
