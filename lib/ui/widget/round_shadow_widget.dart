import 'package:flutter/material.dart';
import 'package:flutter_base_app/ui/widget/shadow_widget.dart';

class RoundShadowWidget extends StatelessWidget {
  const RoundShadowWidget({
    required this.child,
    this.shadowColor,
    this.height,
    this.width,
    this.shadows,
  });

  final double? height;

  final double? width;

  final Color? shadowColor;

  final Widget child;

  final List<BoxShadow>? shadows;

  @override
  Widget build(BuildContext context) {
    return ShadowWidget(
      height: height,
      width: width,
      shadows: shadows,
      shape: BoxShape.circle,
      shadowColor: shadowColor,
      child: child,
    );
  }
}
