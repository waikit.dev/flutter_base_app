import 'package:flutter/material.dart';
import 'package:qr_flutter/qr_flutter.dart';

class QrImageWidget extends StatelessWidget {
  const QrImageWidget({
    required this.data,
    this.size,
    this.embedded,
  });

  final double? size;

  final String data;

  final Widget? embedded;

  @override
  Widget build(BuildContext context) {
    return Stack(
      alignment: Alignment.center,
      children: [
        QrImage(
          data: data,
          padding: EdgeInsets.zero,
          size: size,
          errorCorrectionLevel: QrErrorCorrectLevel.H,
        ),
        embedded ?? Container(),
      ],
    );
  }
}
