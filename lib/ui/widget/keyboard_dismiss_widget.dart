import 'package:flutter/material.dart';

class KeyboardDismissWidget extends StatelessWidget {
  const KeyboardDismissWidget({
    required this.child,
  });

  final Widget child;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        WidgetsBinding.instance?.focusManager.primaryFocus?.unfocus();
      },
      child: child,
    );
  }
}
