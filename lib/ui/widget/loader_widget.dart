import 'package:flutter/material.dart';
import 'package:flutter_base_app/app/application.dart';
import 'package:flutter_base_app/helpers/screen.dart';

class LoaderWidget extends StatelessWidget {
  const LoaderWidget();

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 64.0.scale(),
      height: 64.0.scale(),
      decoration: BoxDecoration(
        color: R.color.colorDialogBg,
        borderRadius: BorderRadius.circular(64.0.scale()),
      ),
      child: Center(
        child: Container(
          height: 32.0.scale(),
          width: 32.0.scale(),
          child: CircularProgressIndicator(
            color: R.color.colorAccent,
            strokeWidth: 3.0.scale(),
          ),
        ),
      ),
    );
  }
}
