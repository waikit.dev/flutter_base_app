import 'package:flutter/material.dart';
import 'package:flutter_base_app/app/application.dart';

class DividerWidget extends StatelessWidget {
  const DividerWidget({
    this.height = 1.0,
    this.color,
  });

  final double height;

  final Color? color;

  @override
  Widget build(BuildContext context) {
    return Container(
      height: height,
      width: double.infinity,
      color: color ?? R.color.colorDivider,
    );
  }
}
