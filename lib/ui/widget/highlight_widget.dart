import 'package:flutter/material.dart';
import 'package:flutter_base_app/app/application.dart';
import 'package:flutter_base_app/helpers/screen.dart';

class HighlightWidget extends StatelessWidget {
  const HighlightWidget({
    required this.child,
    this.radius,
    this.highlightColor,
    this.padding,
  });

  final double? radius;

  final Color? highlightColor;

  final Widget child;

  final EdgeInsets? padding;

  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.center,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(radius ?? 6.0.scale()),
        color: highlightColor ?? R.color.colorAccent,
      ),
      padding: padding ??
          EdgeInsets.symmetric(
            horizontal: 12.0.scale(),
            vertical: 6.0.scale(),
          ),
      child: child,
    );
  }
}
