import 'package:flutter/material.dart';
import 'package:flutter_base_app/app/application.dart';
import 'package:flutter_base_app/ui/widget/text_widget.dart';
import 'package:flutter_base_app/helpers/screen.dart';

class StatusTextWidget extends StatelessWidget {
  const StatusTextWidget({
    required this.text,
    this.isActive = false,
    this.themeColor,
    this.statusSize,
    this.onMoreInfo,
    this.textStyle,
  });

  final bool isActive;

  final double? statusSize;

  final String text;

  final Color? themeColor;

  final Function? onMoreInfo;

  final TextStyle? textStyle;

  double get _statusSize => statusSize ?? 12.0.scale();

  Color get _themeColor => themeColor ?? R.color.colorAccent;

  Color get _statusColor => isActive ? _themeColor : R.color.colorTransparent;

  double get _iconSize => _statusSize - 2.0.scale();

  Color get _iconColor => isActive ? R.color.colorWhite : _themeColor;

  TextStyle? get _textStyle =>
      textStyle ?? R.textTheme.headline2?.copyWith(color: R.color.colorText, fontSize: 10.0.scale());

  @override
  Widget build(BuildContext context) {
    return InkWell(
      highlightColor: R.color.colorTransparent,
      splashColor: R.color.colorTransparent,
      onTap: () {
        onMoreInfo?.call();
      },
      child: Row(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Container(
            width: _statusSize,
            height: _statusSize,
            decoration: BoxDecoration(
              border: Border.all(color: _themeColor),
              borderRadius: BorderRadius.circular(_statusSize),
              color: _statusColor,
            ),
            child: Icon(
              Icons.done,
              size: _iconSize,
              color: _iconColor,
            ),
          ),
          SizedBox(
            width: 4.0.scale(),
          ),
          TextWidget(
            text: text,
            textStyle: _textStyle,
          ),
        ],
      ),
    );
  }
}
