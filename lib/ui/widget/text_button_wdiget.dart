import 'package:flutter/material.dart';
import 'package:flutter_base_app/app/application.dart';
import 'package:flutter_base_app/ui/widget/button_widget.dart';
import 'package:flutter_base_app/ui/widget/text_widget.dart';

class TextButtonWidget extends StatelessWidget {
  const TextButtonWidget({
    required this.text,
    this.isBorderStyle = false,
    this.isPlainStyle = false,
    this.isWrap = false,
    this.onTap,
    this.themeColor,
    this.splashColor,
    this.height,
    this.width,
    this.buttonGradient,
    this.buttonRadius = 4.0,
    this.padding,
    this.textStyle,
  });

  final String text;

  final bool isBorderStyle;

  final bool isPlainStyle;

  final bool isWrap;

  final double buttonRadius;

  final double? width;

  final double? height;

  final Color? themeColor;

  final Color? splashColor;

  final Gradient? buttonGradient;

  final Function? onTap;

  final EdgeInsets? padding;

  final TextStyle? textStyle;

  Color get _themeColor => themeColor ?? R.color.colorAccent;

  TextStyle? get _textStyle =>
      textStyle ??
      R.textTheme.headline2?.copyWith(color: (isBorderStyle || isPlainStyle) ? _themeColor : R.color.colorWhite);

  @override
  Widget build(BuildContext context) {
    return ButtonWidget(
      onTap: onTap,
      width: width,
      height: height,
      isPlainStyle: isPlainStyle,
      isBorderStyle: isBorderStyle,
      isWrap: isWrap,
      themeColor: _themeColor,
      splashColor: splashColor,
      buttonRadius: buttonRadius,
      buttonGradient: buttonGradient,
      padding: padding,
      child: TextWidget(
        text: text,
        textAlign: TextAlign.center,
        textStyle: _textStyle,
      ),
    );
  }
}
