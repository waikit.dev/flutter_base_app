import 'package:flutter/material.dart';
import 'package:flutter_base_app/app/application.dart';
import 'package:flutter_base_app/helpers/screen.dart';

class PagerIndicatorWidget extends StatefulWidget {
  const PagerIndicatorWidget({
    Key? key,
    required this.pageList,
  }) : super(key: key);

  final List<Widget> pageList;

  @override
  PagerIndicatorWidgetState createState() => PagerIndicatorWidgetState();

  static PagerIndicatorWidgetState? of(BuildContext context) {
    final PagerIndicatorWidgetState? state = context.findAncestorStateOfType<PagerIndicatorWidgetState>();
    return state;
  }
}

class PagerIndicatorWidgetState extends State<PagerIndicatorWidget> {
  final PageController _pageController = PageController();

  int _pageIndex = 0;
  int get pageIndex => _pageIndex;
  int get pageCount => widget.pageList.length;
  bool get isFirstPage => pageIndex == 0;
  bool get isLastPage => pageIndex == pageCount;
  bool get isMultiplePage => pageCount > 1;
  Color indicatorColorAtIndex(int index) => (index <= pageIndex) ? R.color.colorActive : R.color.colorInactive;
  Widget pageAtIndex(int index) => widget.pageList.elementAt(index);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Padding(
          padding: EdgeInsets.fromLTRB(16.0.scale(), 12.0.scale(), 16.0.scale(), 24.0.scale()),
          child: Row(
            children: List.generate(
              pageCount,
              (index) => Row(
                children: [
                  Container(
                    width: 48.0.scale(),
                    height: 10.0.scale(),
                    decoration: BoxDecoration(
                      color: indicatorColorAtIndex(index),
                      borderRadius: BorderRadius.circular(
                        20.0.scale(),
                      ),
                    ),
                  ),
                  SizedBox(
                    width: 6.0.scale(),
                  ),
                ],
              ),
            ),
          ),
        ),
        Expanded(
          child: PageView(
            physics: NeverScrollableScrollPhysics(),
            controller: _pageController,
            children: List.generate(pageCount, (index) => pageAtIndex(index)),
          ),
        )
      ],
    );
  }

  void nextPage({bool animated = false}) {
    if (isLastPage) return;
    setState(() {
      _pageIndex = pageIndex + 1;
    });
    if (animated) {
      _pageController.animateToPage(
        pageIndex,
        duration: Duration(
          milliseconds: 200,
        ),
        curve: Curves.decelerate,
      );
    } else {
      _pageController.jumpToPage(
        pageIndex,
      );
    }
  }

  void prevPage({bool animated = false}) {
    if (isFirstPage) return;
    setState(() {
      _pageIndex = pageIndex - 1;
    });
    if (animated) {
      _pageController.animateToPage(
        pageIndex,
        duration: Duration(
          milliseconds: 200,
        ),
        curve: Curves.decelerate,
      );
    } else {
      _pageController.jumpToPage(
        pageIndex,
      );
    }
  }
}
