import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_base_app/app/toolkit.dart';
import 'package:flutter_base_app/app/application.dart';
import 'package:flutter_base_app/helpers/screen.dart';

class ImageWidget extends StatelessWidget {
  const ImageWidget({
    this.file,
    this.imageUrl,
    this.assetName,
    this.width,
    this.height,
    this.scaleType,
    this.package,
  });

  final double? width;

  final double? height;

  final File? file;

  final String? imageUrl;

  final String? assetName;

  final BoxFit? scaleType;

  final String? package;

  bool get _hasFile => file != null;

  bool get _hasImageUrl => !Toolkit.strings.isNullOrEmpty(imageUrl);

  bool get _hasAssetName => !Toolkit.strings.isNullOrEmpty(assetName);

  @override
  Widget build(BuildContext context) {
    if (_hasFile) {
      return Image.file(
        file!,
        fit: scaleType ?? BoxFit.cover,
        height: height,
        width: width,
      );
    }

    if (_hasImageUrl) {
      return Image.network(
        imageUrl!,
        fit: scaleType ?? BoxFit.cover,
        height: height,
        width: width,
        loadingBuilder: (context, child, event) {
          if (event == null) return child;
          return Container(
            color: R.color.colorHint.withOpacity(0.2),
            height: height,
            width: width,
            child: Center(
              child: CupertinoActivityIndicator(),
            ),
          );
        },
      );
    }

    if (_hasAssetName) {
      return Image.asset(
        assetName!,
        width: width,
        height: height,
        fit: scaleType ?? BoxFit.cover,
        package: package,
      );
    }

    return Container(
      color: R.color.colorBlack.withOpacity(0.05),
      height: height,
      width: width,
      child: Center(
        child: Icon(
          Icons.image,
          color: R.color.colorHint,
          size: 36.0.scale(),
        ),
      ),
    );
  }
}
