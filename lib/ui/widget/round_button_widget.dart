import 'package:flutter/material.dart';
import 'package:flutter_base_app/ui/widget/button_widget.dart';
import 'package:flutter_base_app/helpers/screen.dart';

class RoundButtonWidget extends StatelessWidget {
  const RoundButtonWidget({
    required this.child,
    this.size,
    this.themeColor,
    this.splashColor,
    this.onTap,
    this.gradient,
  });

  final double? size;

  final Color? themeColor;

  final Color? splashColor;

  final Widget child;

  final Function? onTap;

  final Gradient? gradient;

  @override
  Widget build(BuildContext context) {
    return ButtonWidget(
      child: child,
      onTap: onTap,
      height: size ?? 48.0.scale(),
      width: size ?? 48.0.scale(),
      themeColor: themeColor,
      splashColor: splashColor,
      buttonGradient: gradient,
      buttonRadius: size ?? 48.0.scale(),
    );
  }
}
