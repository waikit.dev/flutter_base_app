import 'package:flutter/material.dart';
import 'package:flutter_base_app/app/application.dart';
import 'package:flutter_base_app/ui/widget/appbar_widget.dart';

class SinglePageWidget extends StatelessWidget {
  const SinglePageWidget({
    required this.child,
    required this.appBar,
  });

  final Widget child;

  final AppbarWidget appBar;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: appBar,
      body: SafeArea(
        top: false,
        child: child,
      ),
    );
  }
}
