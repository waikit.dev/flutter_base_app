import 'package:flutter/material.dart';
import 'package:flutter_base_app/app/toolkit.dart';
import 'package:flutter_base_app/app/application.dart';
import 'package:flutter_base_app/helpers/screen.dart';
import 'package:flutter_base_app/ui/widget/text_widget.dart';

class AppbarWidget extends StatelessWidget implements PreferredSizeWidget {
  const AppbarWidget({
    this.title,
    this.canBack = true,
    this.widget,
    this.actionList,
  });

  final bool canBack;

  final String? title;

  final Widget? widget;

  final List<Widget>? actionList;

  @override
  Widget build(BuildContext context) {
    return Container(
      width: Toolkit.screen.screenWidth,
      decoration: new BoxDecoration(
        color: R.color.colorPrimary,
        boxShadow: [
          new BoxShadow(
            color: Colors.grey,
            blurRadius: 20.0,
            spreadRadius: 1.0,
          )
        ],
      ),
      child: SafeArea(
        bottom: false,
        child: Container(
          height: Toolkit.screen.appBarHeight,
          child: Stack(
            children: [
              Align(
                alignment: Alignment.centerLeft,
                child: Visibility(
                  visible: canBack,
                  child: InkWell(
                    child: Container(
                      height: Toolkit.screen.appBarHeight,
                      width: Toolkit.screen.appBarHeight,
                      child: Icon(
                        Icons.arrow_back,
                        color: R.color.colorWhite,
                        size: 20.0.scale(),
                      ),
                    ),
                    onTap: () {
                      Toolkit.navigator.goBack();
                    },
                  ),
                ),
              ),
              Align(
                alignment: Alignment.center,
                child: widget ??
                    TextWidget(
                      text: title,
                      textAlign: TextAlign.center,
                      textStyle: R.textTheme.headline2?.copyWith(
                        color: R.color.colorTitle,
                        fontSize: 20.0.scale(),
                      ),
                    ),
              ),
              Align(
                alignment: Alignment.centerRight,
                child: Visibility(
                  visible: (actionList?.length ?? 0) > 0,
                  child: Row(
                    mainAxisSize: MainAxisSize.min,
                    children: actionList ?? [],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  @override
  Size get preferredSize => Size(Toolkit.screen.screenWidth, Toolkit.screen.appBarHeight);
}
