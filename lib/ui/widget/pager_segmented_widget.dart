import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_base_app/app/application.dart';
import 'package:flutter_base_app/app/toolkit.dart';
import 'package:flutter_base_app/helpers/screen.dart';

class PagerSegmentWidget extends StatefulWidget {
  const PagerSegmentWidget({
    Key? key,
    required this.pageList,
    required this.titleList,
  }) : super(key: key);

  final List<String> titleList;

  final List<Widget> pageList;

  @override
  _PagerSegmentWidgetState createState() => _PagerSegmentWidgetState();
}

class _PagerSegmentWidgetState extends State<PagerSegmentWidget> {
  _PagerSegmentWidgetState();

  int _pageIndex = 0;

  int get pageIndex => _pageIndex;

  bool get isMultiplePage => pageCount > 1;

  int get pageCount => (widget.pageList.length);

  String textAtIndex(int index) => widget.titleList.elementAt(index);

  Widget pageAtIndex(int index) => widget.pageList.elementAt(index);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          padding: EdgeInsets.only(
            left: 16.0.scale(),
            right: 16.0.scale(),
            top: 16.0.scale(),
          ),
          width: Toolkit.screen.screenWidth,
          child: CupertinoSegmentedControl<int>(
            selectedColor: R.color.colorAccent,
            borderColor: R.color.colorAccent,
            unselectedColor: R.color.colorWhite,
            children: widget.titleList
                .map((element) => Padding(
                    padding: EdgeInsets.all(6.0.scale()),
                    child: Text(
                      element,
                      style: TextStyle(fontSize: 14.0.scale()),
                    )))
                .toList()
                .asMap(),
            padding: EdgeInsets.all(0.0.scale()),
            onValueChanged: (int val) {
              setState(() {
                _pageIndex = val;
              });
            },
            groupValue: pageIndex,
          ),
        ),
        SizedBox(
          height: 16.0.scale(),
        ),
        Expanded(
          child: pageAtIndex(pageIndex),
        ),
      ],
    );
  }
}
