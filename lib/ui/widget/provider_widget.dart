import 'package:flutter/material.dart';
import 'package:flutter_base_app/app/application.dart';
import 'package:flutter_base_app/app/controllers.dart';
import 'package:flutter_base_app/app/toolkit.dart';
import 'package:flutter_base_app/controller/controller.dart';
import 'package:flutter_base_app/ui/widget/text_widget.dart';
import 'package:get/get.dart';

class ProviderWidget<T extends Controller> extends StatefulWidget {
  const ProviderWidget({
    required this.builder,
    this.tag,
    this.onReady,
    this.onBindReady,
    this.controller,
    this.permanent = false,
  });

  final T? controller;

  final String? tag;

  final bool permanent;

  final Function(T)? onReady;

  final Function(T)? onBindReady;

  final Widget Function(T) builder;

  @override
  _ProviderWidgetState<T> createState() => _ProviderWidgetState<T>();
}

class _ProviderWidgetState<T extends Controller> extends State<ProviderWidget<T>> {
  _ProviderWidgetState();

  T? controller;

  @override
  void initState() {
    super.initState();
    controller = Controllers.get<T>(tag: widget.tag) ?? widget.controller;

    if (controller == null) return;

    widget.onReady?.call(controller!);

    WidgetsBinding.instance?.addPostFrameCallback((timeStamp) {
      widget.onBindReady?.call(controller!);
    });
  }

  @override
  Widget build(BuildContext context) {
    return controller == null
        ? _ControllerNullWidget<T>()
        : GetBuilder<T>(
            tag: widget.tag,
            init: controller,
            builder: widget.builder,
            autoRemove: !widget.permanent,
          );
  }
}

class _ControllerNullWidget<T> extends StatelessWidget {
  _ControllerNullWidget();

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        Icon(
          Icons.error,
          size: Toolkit.screen.screenWidth / 2,
          color: R.color.colorAccent,
        ),
        SizedBox(
          height: 12.0,
        ),
        TextWidget(
          text: Toolkit.logger.message(
            debug: '\"$T\" is missing !!',
            release: R.label.something_went_wrong,
          ),
          textStyle: R.textTheme.headline3,
        ),
      ],
    );
  }
}
