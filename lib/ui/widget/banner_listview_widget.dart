import 'package:flutter/material.dart';

class BannerListViewWidget extends StatefulWidget {
  const BannerListViewWidget({
    required this.bannerWidget,
    required this.contentWidget,
    this.bannerHeight = 200.0,
  });

  final double bannerHeight;

  final Widget bannerWidget;

  final Widget contentWidget;

  @override
  _BannerListViewWidgetState createState() => _BannerListViewWidgetState();
}

class _BannerListViewWidgetState extends State<BannerListViewWidget> {
  _BannerListViewWidgetState();

  final ScrollController _scrollController = ScrollController();

  double _initialHeight = 0.0;

  double _bannerHeight = 0.0;

  @override
  void initState() {
    super.initState();

    _initialHeight = widget.bannerHeight;
    _bannerHeight = _initialHeight;

    _scrollController.addListener(() {
      double offset = _scrollController.offset;
      double height = _initialHeight + (offset * -1);
      if (offset > 0 && offset > (_initialHeight / 2)) return;
      setState(() {
        _bannerHeight = height;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        ListView(
          padding: EdgeInsets.zero,
          controller: _scrollController,
          children: [
            Container(
              height: _initialHeight,
            ),
            widget.contentWidget,
          ],
        ),
        IgnorePointer(
          child: Container(
            height: _bannerHeight,
            child: widget.bannerWidget,
          ),
        ),
      ],
    );
  }
}
