import 'package:flutter/material.dart';
import 'package:flutter_base_app/app/application.dart';
import 'package:flutter_base_app/ui/widget/image_widget.dart';
import 'package:flutter_base_app/ui/widget/textfield_widget.dart';
import 'package:flutter_base_app/helpers/screen.dart';

typedef TextFieldWidget BuilderFunction(bool isReveal, Function onToggle);

class PasswordTextFieldWidget extends StatefulWidget {
  const PasswordTextFieldWidget({
    required this.controller,
    this.label,
    this.hint,
  });

  final String? label;

  final String? hint;

  final TextEditingController controller;

  @override
  _PasswordTextFieldWidgetState createState() => _PasswordTextFieldWidgetState();
}

class _PasswordTextFieldWidgetState extends State<PasswordTextFieldWidget> {
  _PasswordTextFieldWidgetState();

  bool _isReveal = false;

  String get _assetName => _isReveal ? R.image.iconShowPassword : R.image.iconHidePassword;

  @override
  Widget build(BuildContext context) {
    return TextFieldWidget(
      maxLines: 1,
      label: widget.label ?? '',
      hint: widget.hint ?? '',
      isObscure: !_isReveal,
      controller: widget.controller,
      suffixWidget: GestureDetector(
        child: Padding(
          padding: EdgeInsets.symmetric(
            horizontal: 8.0.scale(),
          ),
          child: ColorFiltered(
            colorFilter: ColorFilter.mode(
              R.color.colorAccent,
              BlendMode.srcIn,
            ),
            child: ImageWidget(
              assetName: _assetName,
              height: 20.0.scale(),
              width: 20.0.scale(),
            ),
          ),
        ),
        onTap: () {
          setState(() {
            _isReveal = !_isReveal;
          });
        },
      ),
    );
  }
}
