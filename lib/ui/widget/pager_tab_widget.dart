import 'package:flutter/material.dart';
import 'package:flutter_base_app/app/application.dart';
import 'package:flutter_base_app/helpers/screen.dart';

class PagerTabWidget extends StatelessWidget {
  const PagerTabWidget({
    required this.pageList,
    required this.titleList,
  });

  final List<String> titleList;

  final List<Widget> pageList;

  int get _pageCount => pageList.length;

  String _textAtIndex(int index) => titleList.elementAt(index);

  Widget _pageAtIndex(int index) => pageList.elementAt(index);

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: _pageCount,
      child: Column(
        children: [
          Visibility(
            visible: _pageCount > 1,
            child: TabBar(
              indicatorColor: R.color.colorAccent,
              indicatorWeight: 1.5.scale(),
              labelColor: R.color.colorAccent,
              unselectedLabelColor: R.color.colorText,
              labelStyle: TextStyle(
                fontWeight: FontWeight.normal,
                fontSize: 14.0.scale(),
              ),
              unselectedLabelStyle: TextStyle(
                fontWeight: FontWeight.normal,
                fontSize: 14.0.scale(),
              ),
              tabs: List.generate(
                _pageCount,
                (index) => Tab(
                  text: _textAtIndex(index),
                ),
              ),
            ),
          ),
          Expanded(
            child: TabBarView(
              children: List.generate(
                _pageCount,
                (index) => _pageAtIndex(index),
              ),
            ),
          )
        ],
      ),
    );
  }
}
