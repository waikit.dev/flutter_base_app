import 'package:flutter/material.dart';
import 'package:flutter_base_app/app/application.dart';
import 'package:flutter_base_app/helpers/screen.dart';

class ButtonWidget extends StatelessWidget {
  const ButtonWidget({
    required this.child,
    this.isBorderStyle = false,
    this.isPlainStyle = false,
    this.isWrap = false,
    this.onTap,
    this.themeColor,
    this.splashColor,
    this.height,
    this.width,
    this.buttonGradient,
    this.buttonRadius = 4.0,
    this.padding,
  });

  /// THE [child] contained by the button;
  /// Can be any widget contains text, icon, image, and etc...
  ///
  final Widget child;

  /// WHETHER this button is color border with transparent body;
  ///
  /// [DEFAULT] => false;
  ///
  final bool isBorderStyle;

  /// WHETHER this button is transparent border and body;
  ///
  /// [DEFAULT] => false;
  ///
  final bool isPlainStyle;

  /// WHETHER this button size depend to [child];
  ///
  final bool isWrap;

  /// DEFINE radius of button corner;
  ///
  /// [DEFAULT] => 4.0;
  ///
  final double buttonRadius;

  /// DEFINE width of button, is nullable;
  ///
  /// [DEFAULT] => double.infinity;
  ///
  /// ALWAYS use [width] if not null;
  ///
  final double? width;

  /// DEFINE height of button, is nullable;
  ///
  /// [DEFAULT] => 48.0;
  ///
  /// ALWAYS use [height] if not null;
  ///
  final double? height;

  /// DEFINE theme color of button;
  ///
  /// [DEFAULT] => colorAccent;
  ///
  /// IF [isBorderStyle] == true, [themeColor] is used on border and splash color;
  /// IF [isPlainStyle] == true, [themeColor] is used on splash color;
  ///
  final Color? themeColor;

  /// DEFINE splash color of button;
  ///
  /// ALWAYS use [splashColor] if not null;
  ///
  /// IF [splashColor] == null && [isBorderStyle] == true, use [themeColor] as splash color;
  /// IF [splashColor] == null && [isBorderStyle] == false, use [colorBlack] as splash color;
  ///
  final Color? splashColor;

  /// DEFINE padding of button;
  ///
  /// ALWAYS use [padding] if not return null;
  ///
  /// IF [padding] == null && [isWrap] == true, default to horizontal 24.0 and vertical 8.0;
  /// IF [padding] == null && [isWrap] == false, default to zero;
  ///
  final EdgeInsets? padding;

  /// DEFINE function on button.
  ///
  final Function? onTap;

  /// DEFINE gradient on button.
  ///
  final Gradient? buttonGradient;

  Color get _splashColor =>
      splashColor ??
      (isBorderStyle ? (themeColor ?? R.color.colorAccent).withOpacity(0.1) : R.color.colorBlack.withOpacity(0.1));

  Color get _borderColor => isPlainStyle ? R.color.colorTransparent : themeColor ?? R.color.colorAccent;

  Color get _bodyColor =>
      (isPlainStyle || isBorderStyle) ? R.color.colorTransparent : themeColor ?? R.color.colorAccent;

  EdgeInsets get _padding =>
      padding ??
      EdgeInsets.symmetric(
        horizontal: isWrap ? 24.0.scale() : 0.0.scale(),
        vertical: isWrap ? 8.0.scale() : 0.0.scale(),
      );

  double? get _width => width ?? (isWrap ? null : double.infinity);

  double? get _height => height ?? (isWrap ? null : 48.0.scale());

  @override
  Widget build(BuildContext context) {
    return Material(
      elevation: 0.0,
      color: R.color.colorTransparent,
      child: InkWell(
        highlightColor: R.color.colorTransparent,
        borderRadius: BorderRadius.circular(buttonRadius),
        splashColor: _splashColor,
        child: Ink(
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(buttonRadius),
            border: Border.all(color: _borderColor),
            color: _bodyColor,
            gradient: buttonGradient,
          ),
          child: Container(
            width: _width,
            height: _height,
            alignment: Alignment.center,
            padding: _padding,
            child: child,
          ),
        ),
        onTap: () {
          onTap?.call();
        },
      ),
    );
  }
}
