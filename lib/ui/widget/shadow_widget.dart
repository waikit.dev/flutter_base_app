import 'package:flutter/material.dart';
import 'package:flutter_base_app/app/application.dart';

class ShadowWidget extends StatelessWidget {
  const ShadowWidget({
    required this.child,
    this.shadowColor,
    this.height,
    this.width,
    this.shape,
    this.shadows,
  });

  final double? height;

  final double? width;

  final Color? shadowColor;

  final Widget child;

  final BoxShape? shape;

  final List<BoxShadow>? shadows;

  @override
  Widget build(BuildContext context) {
    return Container(
      width: width,
      height: height,
      decoration: BoxDecoration(
        shape: shape ?? BoxShape.rectangle,
        boxShadow: shadows ??
            [
              BoxShadow(
                color: R.color.colorBlack.withOpacity(0.5),
                blurRadius: 6.0,
                spreadRadius: 2.0,
                offset: Offset(0, 4.0),
              )
            ],
      ),
      child: child,
    );
  }
}
