import 'package:flutter/material.dart';
import 'package:flutter_base_app/app/toolkit.dart';
import 'package:flutter_base_app/app/application.dart';
import 'package:flutter_base_app/ui/widget/textfield_widget.dart';

class TextFieldSelectionWidget extends StatelessWidget {
  const TextFieldSelectionWidget({
    Key? key,
    required this.controller,
    this.label,
    this.hint,
    this.isRequired = false,
    this.inputTextStyle,
    this.hintTextStyle,
    this.labelTextStyle,
    this.onSelection,
  }) : super(key: key);

  final bool isRequired;

  final String? label;

  final String? hint;

  final TextStyle? inputTextStyle;

  final TextStyle? hintTextStyle;

  final TextStyle? labelTextStyle;

  final Function? onSelection;

  final TextEditingController controller;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      splashColor: R.color.colorTransparent,
      highlightColor: R.color.colorTransparent,
      onTap: () {
        Toolkit.screen.unFocus(context);
        onSelection?.call();
      },
      child: TextFieldWidget(
        isEnable: false,
        isInteractive: false,
        hint: hint ?? '',
        label: label ?? '',
        controller: controller,
        isRequired: isRequired ?? false,
        suffixWidget: Icon(
          Icons.arrow_drop_down,
          color: R.color.colorBlack,
        ),
        inputTextStyle: inputTextStyle,
        hintTextStyle: hintTextStyle,
        labelTextStyle: labelTextStyle,
      ),
    );
  }
}
