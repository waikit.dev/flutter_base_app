import 'package:flutter/material.dart';
import 'package:flutter_base_app/app/toolkit.dart';
import 'package:flutter_base_app/app/application.dart';
import 'package:flutter_base_app/ui/widget/text_button_wdiget.dart';
import 'package:flutter_base_app/helpers/screen.dart';
import 'package:flutter_base_app/ui/widget/text_widget.dart';

typedef SingleButtonDialogBuilder = Widget Function({
  required String message,
  String? title,
  String? action,
  Function? onAction,
});

class SingleButtonDialog extends StatelessWidget {
  const SingleButtonDialog({
    required this.message,
    this.title,
    this.action,
    this.onAction,
  });

  final String message;

  final String? title;

  final String? action;

  final Function? onAction;

  @override
  Widget build(BuildContext context) {
    return Dialog(
      backgroundColor: R.color.colorDialogBg,
      child: Container(
        padding: EdgeInsets.fromLTRB(16.0.scale(), 10.0.scale(), 16.0.scale(), 10.0.scale()),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(6.0.scale()),
        ),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Visibility(
              visible: !Toolkit.strings.isNullOrEmpty(title),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  TextWidget(
                    text: title ?? '',
                    textStyle: R.textTheme.headline2?.copyWith(color: R.color.colorAccent, fontSize: 12.0.scale()),
                  ),
                  SizedBox(
                    height: 4.0.scale(),
                  ),
                ],
              ),
            ),
            TextWidget(
              text: message,
              textAlign: TextAlign.start,
              textStyle: R.textTheme.headline2?.copyWith(color: R.color.colorText, fontSize: 14.0.scale()),
            ),
            SizedBox(
              height: 10.0.scale(),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                TextButtonWidget(
                  text: action ?? '',
                  isWrap: true,
                  onTap: () {
                    Toolkit.navigator.goBack();
                    onAction?.call();
                  },
                  themeColor: R.color.colorAccent,
                  textStyle: R.textTheme.headline2?.copyWith(color: R.color.colorWhite, fontSize: 12.0.scale()),
                ),
              ],
            )
          ],
        ),
      ),
    );
  }
}
