import 'package:flutter/material.dart';
import 'package:flutter_base_app/app/application.dart';
import 'package:flutter_base_app/app/toolkit.dart';
import 'package:flutter_base_app/ui/widget/text_button_wdiget.dart';
import 'package:flutter_base_app/helpers/screen.dart';
import 'package:flutter_base_app/ui/widget/text_widget.dart';

typedef DualButtonDialogBuilder = Widget Function({
  required String message,
  required Function onYes,
  required Function onNo,
  String? title,
  String? yes,
  String? no,
});

class DualButtonDialog extends StatelessWidget {
  const DualButtonDialog({
    required this.message,
    required this.onYes,
    required this.onNo,
    this.yes,
    this.no,
    this.title,
  });

  final String message;

  final Function onYes;

  final Function onNo;

  final String? title;

  final String? yes;

  final String? no;

  @override
  Widget build(BuildContext context) {
    return Dialog(
      backgroundColor: R.color.colorDialogBg,
      child: Container(
        padding: EdgeInsets.fromLTRB(16.0.scale(), 10.0.scale(), 16.0.scale(), 10.0.scale()),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(6.0.scale()),
        ),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Visibility(
              visible: !Toolkit.strings.isNullOrEmpty(title),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  TextWidget(
                    text: title ?? '',
                    textStyle: R.textTheme.headline2?.copyWith(color: R.color.colorAccent, fontSize: 12.0.scale()),
                  ),
                  SizedBox(
                    height: 4.0.scale(),
                  ),
                ],
              ),
            ),
            TextWidget(
              text: message,
              textStyle: R.textTheme.headline2?.copyWith(color: R.color.colorText, fontSize: 14.0.scale()),
              textAlign: TextAlign.start,
            ),
            SizedBox(
              height: 10.0.scale(),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                TextButtonWidget(
                  text: no ?? R.label.no,
                  isPlainStyle: true,
                  isWrap: true,
                  textStyle: R.textTheme.headline2?.copyWith(color: R.color.colorAccent, fontSize: 12.0.scale()),
                  onTap: () {
                    Toolkit.navigator.goBack();
                    onNo.call();
                  },
                ),
                SizedBox(
                  width: 4.0.scale(),
                ),
                TextButtonWidget(
                  text: yes ?? R.label.yes,
                  isWrap: true,
                  themeColor: R.color.colorAccent,
                  textStyle: R.textTheme.headline2?.copyWith(color: R.color.colorWhite, fontSize: 12.0.scale()),
                  onTap: () {
                    Toolkit.navigator.goBack();
                    onYes.call();
                  },
                ),
              ],
            )
          ],
        ),
      ),
    );
  }
}
