import 'package:flutter/material.dart';
import 'package:flutter_base_app/app/toolkit.dart';
import 'package:flutter_base_app/app/application.dart';
import 'package:flutter_base_app/helpers/screen.dart';
import 'package:flutter_base_app/ui/widget/text_widget.dart';

class SelectionDialog extends StatelessWidget {
  const SelectionDialog({
    required this.title,
    required this.children,
  });

  final String title;

  final List<Widget> children;

  @override
  Widget build(BuildContext context) {
    return Container(
      height: Toolkit.screen.screenHeight * 0.6,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Padding(
            padding: EdgeInsets.symmetric(vertical: 12.0.scale(), horizontal: 16.0.scale()),
            child: TextWidget(
              text: title ?? '',
              textStyle: R.textTheme.headline2?.copyWith(color: R.color.colorAccent, fontSize: 16.0.scale()),
            ),
          ),
          Container(
            height: 4.0.scale(),
            color: R.color.colorDivider,
          ),
          Expanded(
            child: ListView(
              padding: EdgeInsets.all(16.0.scale()),
              shrinkWrap: true,
              children: children,
            ),
          )
        ],
      ),
    );
  }
}

class SelectionItemWidget extends StatelessWidget {
  final String text;
  final bool isSelect;

  const SelectionItemWidget({
    required this.text,
    required this.isSelect,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(bottom: 8.0.scale()),
      padding: EdgeInsets.symmetric(horizontal: 16.0.scale(), vertical: 12.0.scale()),
      decoration: BoxDecoration(
        border: Border.all(color: isSelect ? R.color.colorActive : R.color.colorInactive),
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          TextWidget(
            text: text ?? '',
          ),
          Visibility(
              visible: isSelect,
              child: Icon(
                Icons.done,
                color: R.color.colorActive,
              )),
        ],
      ),
    );
  }
}
