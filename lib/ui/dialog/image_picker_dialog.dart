import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_base_app/app/toolkit.dart';
import 'package:flutter_base_app/app/application.dart';
import 'package:flutter_base_app/helpers/screen.dart';

import 'package:flutter_base_app/ui/widget/text_widget.dart';

class ImagePickerDialog extends StatelessWidget {
  const ImagePickerDialog({
    required this.onGetImage,
  });

  final Function(File) onGetImage;

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      mainAxisAlignment: MainAxisAlignment.end,
      children: [
        Column(
          mainAxisAlignment: MainAxisAlignment.end,
          children: [
            InkWell(
              child: Container(
                width: Toolkit.screen.screenWidth,
                padding: EdgeInsets.all(16.0.scale()),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Icon(Icons.image),
                    SizedBox(
                      width: 10.0.scale(),
                    ),
                    TextWidget(
                      text: R.label.choose_from_gallery,
                    ),
                  ],
                ),
              ),
              onTap: () async {
                Toolkit.navigator.goBack();
                Toolkit.image.chooseFromGallery(
                  onGetImage: onGetImage,
                );
              },
            ),
            InkWell(
              child: Container(
                  width: Toolkit.screen.screenWidth,
                  padding: EdgeInsets.all(16.0.scale()),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Icon(Icons.camera),
                      SizedBox(
                        width: 10.0.scale(),
                      ),
                      TextWidget(
                        text: R.label.take_from_camera,
                      ),
                    ],
                  )),
              onTap: () async {
                Toolkit.navigator.goBack();
                Toolkit.image.takeFromCamera(
                  onGetImage: onGetImage,
                );
              },
            ),
            Container(
              height: 4.0.scale(),
              color: R.color.colorDivider,
            ),
            InkWell(
              child: Container(
                width: Toolkit.screen.screenWidth,
                padding: EdgeInsets.all(16.0.scale()),
                child: TextWidget(
                  text: R.label.close,
                  textAlign: TextAlign.center,
                  textStyle: R.textTheme.headline5?.copyWith(color: R.color.colorAccent, fontSize: 16.0.scale()),
                ),
              ),
              onTap: () {
                Toolkit.navigator.goBack();
              },
            )
          ],
        )
      ],
    );
  }
}
