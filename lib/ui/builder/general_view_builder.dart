import 'package:flutter/material.dart';
import 'package:flutter_base_app/app/application.dart';
import 'package:flutter_base_app/ui/dialog/dual_button_dialog.dart';
import 'package:flutter_base_app/ui/dialog/single_button_dialog.dart';
import 'package:flutter_base_app/controller/view/view_state_widget.dart';
import 'package:flutter_base_app/helpers/screen.dart';

mixin GeneralViewBuilder {
  static SingleButtonDialogBuilder singleButtonDialog = ({
    required String message,
    String? title,
    String? action,
    Function? onAction,
  }) =>
      SingleButtonDialog(
        title: title ?? '',
        message: message,
        action: action ?? R.label.ok,
        onAction: onAction,
      );

  static SingleButtonDialogBuilder errorDialog = ({
    required String message,
    String? title,
    String? action,
    Function? onAction,
  }) =>
      SingleButtonDialog(
        title: title ?? R.label.warning,
        message: message,
        action: action ?? R.label.ok,
        onAction: onAction,
      );

  static SingleButtonDialogBuilder successDialog = ({
    required String message,
    String? title,
    String? action,
    Function? onAction,
  }) =>
      SingleButtonDialog(
        title: title ?? R.label.success,
        message: message,
        action: action ?? R.label.ok,
        onAction: onAction,
      );

  static DualButtonDialogBuilder dualButtonDialog = ({
    String? title,
    String? yes,
    String? no,
    required String message,
    required Function onYes,
    required Function onNo,
  }) =>
      DualButtonDialog(
        title: title ?? '',
        message: message,
        yes: yes ?? R.label.yes,
        no: no ?? R.label.no,
        onYes: onYes,
        onNo: onNo,
      );

  static ViewStateWidgetBuilder emptyStateWidget = ({
    String? title,
    String? action,
    Function? onAction,
  }) =>
      ViewStateWidget(
        title: title ?? R.label.no_data,
        action: action ?? R.label.click_here_to_retry,
        onAction: onAction,
      );

  static ViewStateWidgetBuilder errorStateWidget = ({
    String? title,
    String? action,
    Function? onAction,
  }) =>
      ViewStateWidget(
        title: title,
        action: action ?? R.label.click_here_to_retry,
        onAction: onAction,
      );

  static ViewStateWidgetBuilder unAuthStateWidget = ({
    String? title,
    String? action,
    Function? onAction,
  }) =>
      ViewStateWidget(
        title: title ?? R.label.unauthorised,
        action: action ?? R.label.click_here_to_login_again,
        onAction: onAction,
      );

  static Widget busyStateWidget = Center(
    child: SizedBox(
      child: CircularProgressIndicator(
        color: R.color.colorAccent,
        strokeWidth: 3.0.scale(),
      ),
      height: 24.0.scale(),
      width: 24.0.scale(),
    ),
  );
}
