import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_base_app/app/application.dart';
import 'package:get/get.dart';

class Dialog {
  Dialog();

  Future showDialog({
    @required Widget? child,
    bool canDismiss = false,
  }) async {
    await Get.dialog(
      WillPopScope(
        onWillPop: () => Future.value(canDismiss),
        child: child ?? Container(),
      ),
      barrierDismissible: canDismiss,
      transitionDuration: Duration(milliseconds: 200),
      transitionCurve: Curves.decelerate,
      useSafeArea: false,
    );
  }

  Future showBottomSheet({
    @required Widget? child,
    bool canDismiss = false,
    Color? backgroundColor,
  }) async {
    await Get.bottomSheet(
      BottomSheet(
        builder: (context) {
          return Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              child ?? Container(),
              Container(
                height: Get.mediaQuery.padding.bottom,
                color: R.color.colorDialogBg,
              )
            ],
          );
        },
        enableDrag: false,
        onClosing: () {},
        backgroundColor: backgroundColor ?? R.color.colorDialogBg,
      ),
      isDismissible: canDismiss,
    );
  }
}
