import 'package:encrypt/encrypt.dart';
import 'package:flutter_base_app/app/toolkit.dart';
import 'package:pointycastle/asymmetric/api.dart';

class Encrypt {
  Encrypt();

  String? aesEncrypt(String plainText, String? secretKey) {
    assert(!Toolkit.strings.isNullOrEmpty(secretKey));
    if (secretKey == null) return null;
    try {
      Key key = Key.fromUtf8(secretKey);
      IV iv = IV.fromLength(16);
      Encrypter encrypter = Encrypter(AES(key));
      Encrypted encrypted = encrypter.encrypt(plainText, iv: iv);
      return encrypted.base64;
    } on Exception {
      return null;
    }
  }

  String? aesDecrypt(String encryptedText, String? secretKey) {
    assert(!Toolkit.strings.isNullOrEmpty(secretKey));
    if (secretKey == null) return null;
    try {
      Key key = Key.fromUtf8(secretKey);
      IV iv = IV.fromLength(16);
      Encrypter encrypter = Encrypter(AES(key));
      String decrypted = encrypter.decrypt(Encrypted.fromBase64(encryptedText), iv: iv);
      return decrypted;
    } on Exception {
      return null;
    }
  }

  String? rsaEncryptWithPublicKey(String plainText, String? publicKey) {
    assert(!Toolkit.strings.isNullOrEmpty(publicKey));
    if (publicKey == null) return null;
    try {
      RSAKeyParser parser = RSAKeyParser();
      RSAAsymmetricKey? pubKey = parser.parse(publicKey);
      Encrypter encrypter = Encrypter(RSA(publicKey: (pubKey as RSAPublicKey?), encoding: RSAEncoding.PKCS1));
      Encrypted encrypted = encrypter.encrypt(plainText);
      return encrypted.base64;
    } on Exception {
      return null;
    }
  }

  String? rsaDecryptWithPrivateKey(String encryptedText, String? privateKey) {
    assert(!Toolkit.strings.isNullOrEmpty(privateKey));
    if (privateKey == null) return null;
    try {
      RSAKeyParser parser = RSAKeyParser();
      RSAAsymmetricKey? privKey = parser.parse(privateKey);
      Encrypter encrypter = Encrypter(RSA(privateKey: (privKey as RSAPrivateKey?), encoding: RSAEncoding.PKCS1));
      Encrypted encrypted = Encrypted.fromBase64(encryptedText);
      return encrypter.decrypt(encrypted);
    } on Exception {
      return null;
    }
  }
}
