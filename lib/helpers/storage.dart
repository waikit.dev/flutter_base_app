import 'package:flutter_base_app/app/toolkit.dart';
import 'package:shared_preferences/shared_preferences.dart';

class Storage {
  Storage();

  Future<SharedPreferences> _sharedPreference = SharedPreferences.getInstance();

  Future saveString(String key, String? value) async {
    if (Toolkit.strings.isNullOrEmpty(value)) return;
    if (value != null) (await _sharedPreference).setString(key, value);
  }

  Future<String> readString(String key, {String defaultVal = ''}) async {
    return (await _sharedPreference).getString(key) ?? defaultVal;
  }

  Future saveBool(String key, bool value) async {
    (await _sharedPreference).setBool(key, value);
  }

  Future<bool> readBool(String key, {bool defaultVal = false}) async {
    return (await _sharedPreference).getBool(key) ?? defaultVal;
  }

  Future<bool> hasKey(String key) async {
    return (await _sharedPreference).containsKey(key);
  }

  Future deleteKey(String key) async {
    (await _sharedPreference).remove(key);
  }

  Future clearAll() async {
    (await _sharedPreference).clear();
  }
}
