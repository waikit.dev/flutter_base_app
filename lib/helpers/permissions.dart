import 'package:permission_handler/permission_handler.dart';

class Permissions {
  Permissions();

  void requestPermission(
    Permission permission, {
    Function? onPermanentDeny,
    Function? onGrant,
    Function? onDeny,
  }) async {
    PermissionStatus status = await permission.status;

    if (status.isPermanentlyDenied) {
      onPermanentDeny?.call();
    } else if (status.isGranted || status.isLimited) {
      onGrant?.call();
    } else {
      PermissionStatus requestStatus = await permission.request();
      if (requestStatus.isGranted || status.isLimited) {
        onGrant?.call();
      } else {
        onDeny?.call();
      }
    }
  }

  void openSetting() {
    openAppSettings();
  }
}
