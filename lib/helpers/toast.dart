import 'package:oktoast/oktoast.dart';

class Toast {
  Toast();

  void message({required String message, int seconds = 2}) {
    showToast(
      message,
      position: ToastPosition.bottom,
      duration: Duration(seconds: seconds),
    );
  }

  void dismissAllToast() {
    dismissAllToast();
  }
}
