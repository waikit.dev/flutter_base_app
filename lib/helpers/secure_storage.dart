import 'package:flutter_base_app/app/toolkit.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';

class SecureStorage {
  SecureStorage();

  FlutterSecureStorage _secureStorage = FlutterSecureStorage();

  String? _privateKey;

  String? get privateKey => _privateKey;

  String? _publicKey;

  String? get publicKey => _publicKey;

  String? _secretKey;

  String? get secretKey => _secretKey;

  void setup({String? publicKey, String? privateKey, String? secretKey}) {
    _publicKey = publicKey;
    _privateKey = privateKey;
    _secretKey = secretKey;
  }

  Future saveString(String key, String? value) async {
    if (Toolkit.strings.isNullOrEmpty(value)) return;
    await _secureStorage.write(key: key, value: value);
  }

  Future<String> readString(String key, {String defaultVal = ''}) async {
    return await _secureStorage.read(key: key) ?? defaultVal;
  }

  Future saveRsaEncryptedString(String key, String value) async {
    await saveString(key, Toolkit.encrypt.rsaEncryptWithPublicKey(value, publicKey));
  }

  Future<String> readRsaDecryptedString(String key, {String defaultVal = ''}) async {
    try {
      return Toolkit.encrypt.rsaDecryptWithPrivateKey(await readString(key), privateKey) ?? defaultVal;
    } catch (ex) {
      Toolkit.logger.exception(obj: ex);
      return defaultVal;
    }
  }

  Future saveAesEncryptedString(String key, String value) async {
    await saveString(key, Toolkit.encrypt.aesEncrypt(value, secretKey));
  }

  Future<String> readAesDecryptedString(String key, {String defaultVal = ''}) async {
    try {
      return Toolkit.encrypt.aesDecrypt(await readString(key), secretKey) ?? defaultVal;
    } catch (ex) {
      Toolkit.logger.exception(obj: ex);
      return defaultVal;
    }
  }

  Future<bool> hasKey(String key) async {
    return await _secureStorage.containsKey(key: key);
  }

  Future deleteKey(String key) async {
    await _secureStorage.delete(key: key);
  }

  Future clearAll() async {
    await _secureStorage.deleteAll();
  }
}

class WebSecureStorage extends SecureStorage {
  WebSecureStorage();

  Future saveString(String key, String? value) async {
    await Toolkit.storage.saveString(key, value);
  }

  Future<String> readString(String key, {String defaultVal = ''}) async {
    return await Toolkit.storage.readString(key, defaultVal: defaultVal);
  }

  Future<bool> hasKey(String key) async {
    return await Toolkit.storage.hasKey(key);
  }

  Future deleteKey(String key) async {
    await Toolkit.storage.deleteKey(key);
  }

  Future clearAll() async {
    await Toolkit.storage.clearAll();
  }
}
