import 'package:flutter_base_app/app/application.dart';
import 'package:intl/intl.dart';

class Calendar {
  Calendar();

  DateTime get currentDateTime => DateTime.now();

  DateTime get currentDate => DateTime(currentDateTime.year, currentDateTime.month, currentDateTime.day);

  String getFriendlyDisplayDateByDateString(String dateStr) {
    DateTime? dateTime = getDateTimeByDateTimeStr(dateStr);

    if (dateTime == null) return '';
    int milliseconds = currentDateTime.difference(dateTime).inMilliseconds;

    Duration duration = Duration(milliseconds: milliseconds);
    if (duration.inSeconds < 60) return '${duration.inSeconds} ${R.label.seconds_ago}';
    if (duration.inMinutes < 60) return '${duration.inMinutes} ${R.label.minutes_ago}';
    if (duration.inHours < 24) return '${duration.inHours} ${R.label.hours_ago}';
    if (duration.inDays < 7) return '${duration.inDays} ${R.label.days_ago}';
    if (duration.inDays >= 7) return '${(duration.inDays / 7).toStringAsFixed(0)} ${R.label.weeks_ago}';

    return '';
  }

  DateTime? getDateTimeByDateTimeStr(String dateStr) {
    return DateTime.tryParse(dateStr);
  }

  String formatDateTime({required String format, required DateTime dateTime}) {
    return DateFormat(format).format(dateTime);
  }
}
