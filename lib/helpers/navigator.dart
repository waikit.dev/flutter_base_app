import 'package:get/get.dart';

typedef PageBuilder = Function();

class Navigator {
  Navigator();

  Future pushName({required String page, dynamic arguments, bool clearStack = false}) async {
    return clearStack
        ? await Get.offAllNamed(page, arguments: arguments)
        : await Get.toNamed(page, arguments: arguments);
  }

  Future pushPage({
    required PageBuilder builder,
    bool clearStack = false,
    Function? bindBuilder,
    Transition? transition,
  }) async {
    return clearStack
        ? await Get.offAll(
            builder,
            transition: transition,
            binding: bindBuilder == null ? null : BindingsBuilder(bindBuilder.call()),
          )
        : await Get.to(
            builder,
            transition: transition,
            binding: bindBuilder == null ? null : BindingsBuilder(bindBuilder.call()),
          );
  }

  void goBack() {
    Get.back();
  }
}
