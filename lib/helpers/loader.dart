import 'package:flutter/material.dart';
import 'package:flutter_base_app/app/toolkit.dart';
import 'package:flutter_base_app/ui/widget/loader_widget.dart';

class Loader {
  Loader();

  bool _isLoading = false;

  bool get isLoading => _isLoading;

  Widget _loadWidget = LoaderWidget();

  void setupLoadWidget(Widget widget) {
    _loadWidget = widget;
  }

  void showLoading() async {
    if (isLoading) return;
    _isLoading = true;

    Toolkit.dialog.showDialog(
      canDismiss: false,
      child: WillPopScope(
        onWillPop: () async {
          _isLoading = false;
          return Future.value(true);
        },
        child: UnconstrainedBox(
          child: _loadWidget,
        ),
      ),
    );
  }

  void hideLoading() {
    if (!isLoading) return;
    _isLoading = false;
    Toolkit.navigator.goBack();
  }
}
