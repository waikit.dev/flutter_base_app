import 'package:get/get_utils/src/platform/platform.dart';

class Platform {
  Platform();

  bool get isWeb => GetPlatform.isWeb;

  bool get isIOS => GetPlatform.isIOS;

  bool get isAndroid => GetPlatform.isAndroid;
}
