import 'package:flutter/foundation.dart';
import 'package:flutter/services.dart';
import 'package:flutter_base_app/app/toolkit.dart';
import 'package:flutter_base_app/constants.dart';

import 'package:url_launcher/url_launcher.dart';
import 'package:share/share.dart';

class Launcher {
  Launcher();

  bool _isValid(String? value) => !Toolkit.strings.isNullOrEmpty(value);

  void launchTel({@required String? tel, Function? onError}) {
    if (_isValid(tel)) {
      Uri uri = Uri(scheme: Constants.tel, path: tel);
      launch(uri.toString());
    } else {
      onError?.call();
    }
  }

  void launchEmail({@required String? mailTo, Function? onError}) {
    if (_isValid(mailTo)) {
      Uri uri = Uri(scheme: Constants.mailto, path: mailTo);
      launch(uri.toString());
    } else {
      onError?.call();
    }
  }

  void shareText({@required String? text, Function? onError}) async {
    if (_isValid(text)) {
      Share.share(text);
    } else {
      onError?.call();
    }
  }

  Future copyText({@required String? text, Function? onError, Function? onCopied}) async {
    if (_isValid(text)) {
      await Clipboard.setData(ClipboardData(text: text));
      onCopied?.call();
    } else {
      onError?.call();
    }
  }
}
