import 'dart:io';

import 'package:dio/dio.dart';
import 'package:flutter_base_app/app/application.dart';

class DioResultModel {
  DioResultModel._();

  int? code;

  String? message;

  dynamic data;

  factory DioResultModel.create({int? code, String? message, dynamic data}) {
    DioResultModel model = DioResultModel._()
      ..code = code
      ..message = message
      ..data = data;
    return model;
  }
}

class MyHttpOverrides extends HttpOverrides {
  MyHttpOverrides();

  @override
  HttpClient createHttpClient(SecurityContext? context) {
    return super.createHttpClient(context)
      ..badCertificateCallback = (X509Certificate cert, String host, int port) => true;
  }
}

mixin HttpResponseCode {
  static const int SUCCESS = 200;

  static const int BAD_REQUEST = 400;

  static const int UNAUTHORISED = 401;

  static const int NOT_FOUND_404 = 404;

  static const int METHOD_NOT_ALLOW = 405;

  static const int INTERNAL_SERVER_ERROR = 500;

  static const int PARSE_ERROR = 1001;

  static const int UNKNOWN_ERROR = 1002;

  static const int CANCEL_REQUEST = 1003;

  static const int CONNECTION_TIMEOUT = 1004;
}

class Https {
  Https();

  bool _isLogEnable = true;

  void setLogEnable(bool enable) => _isLogEnable = enable;

  Dio _getDio({required String baseUrl, required String method, Map<String, dynamic>? headers}) {
    Dio dio = new Dio();
    dio.options.baseUrl = baseUrl;
    dio.options.method = method;
    dio.options.sendTimeout = Duration(seconds: 10).inMilliseconds;
    dio.options.connectTimeout = Duration(seconds: 10).inMilliseconds;
    dio.options.receiveTimeout = Duration(seconds: 10).inMilliseconds;
    if (_isLogEnable) {
      dio.interceptors.add(
        LogInterceptor(
          request: true,
          requestHeader: false,
          responseHeader: false,
          requestBody: true,
          responseBody: true,
          error: true,
        ),
      );
    }
    if (headers != null) {
      dio.interceptors.add(
        InterceptorsWrapper(
          onRequest: (options, handler) {
            headers.forEach((key, value) {
              options.headers[key] = value;
            });
            handler.next(options);
          },
        ),
      );
    }

    return dio;
  }

  Future<DioResultModel> postRequest({
    required String baseUrl,
    required String path,
    Map<String, dynamic>? headers,
    Map<String, dynamic>? data,
  }) async {
    try {
      dynamic response = await _getDio(
        baseUrl: baseUrl,
        method: 'POST',
        headers: headers,
      ).post(path, data: data);
      return _processResponse(response);
    } on DioError catch (e) {
      return _processResponse(e);
    }
  }

  Future<DioResultModel> getRequest({
    required String baseUrl,
    required String path,
    Map<String, dynamic>? headers,
    Map<String, dynamic>? queryParameters,
  }) async {
    try {
      dynamic response = await _getDio(
        baseUrl: baseUrl,
        method: 'GET',
        headers: headers,
      ).get(path, queryParameters: queryParameters);
      return _processResponse(response);
    } on DioError catch (e) {
      return _processResponse(e);
    }
  }

  DioResultModel _processResponse(dynamic dioData) {
    if (dioData is DioError) {
      if (dioData.type == DioErrorType.response) {
        int statusCode = dioData.response?.statusCode ?? HttpResponseCode.UNKNOWN_ERROR;
        switch (statusCode) {
          case HttpResponseCode.BAD_REQUEST:
            return DioResultModel.create(
              code: HttpResponseCode.BAD_REQUEST,
              message: dioData.message,
              data: dioData.response?.data,
            );
          case HttpResponseCode.UNAUTHORISED:
            return DioResultModel.create(
              code: HttpResponseCode.UNAUTHORISED,
              message: dioData.message,
              data: dioData.response?.data,
            );
          case HttpResponseCode.NOT_FOUND_404:
            return DioResultModel.create(
              code: HttpResponseCode.NOT_FOUND_404,
              message: dioData.message,
              data: null,
            );
          case HttpResponseCode.METHOD_NOT_ALLOW:
            return DioResultModel.create(
              code: HttpResponseCode.METHOD_NOT_ALLOW,
              message: dioData.message,
              data: null,
            );
          case HttpResponseCode.INTERNAL_SERVER_ERROR:
            return DioResultModel.create(
              code: HttpResponseCode.INTERNAL_SERVER_ERROR,
              message: dioData.message,
              data: null,
            );
          case HttpResponseCode.UNKNOWN_ERROR:
            return DioResultModel.create(
              code: HttpResponseCode.UNKNOWN_ERROR,
              message: dioData.message,
              data: null,
            );
        }
      } else if (dioData.type == DioErrorType.connectTimeout ||
          dioData.type == DioErrorType.receiveTimeout ||
          dioData.type == DioErrorType.sendTimeout) {
        return DioResultModel.create(
          code: HttpResponseCode.CONNECTION_TIMEOUT,
          message: dioData.message,
          data: null,
        );
      } else if (dioData.type == DioErrorType.other) {
        return DioResultModel.create(
          code: HttpResponseCode.UNKNOWN_ERROR,
          message: dioData.message,
          data: null,
        );
      } else if (dioData.type == DioErrorType.cancel) {
        return DioResultModel.create(
          code: HttpResponseCode.CANCEL_REQUEST,
          message: dioData.message,
          data: null,
        );
      }
    } else if (dioData is Response) {
      return DioResultModel.create(
        code: HttpResponseCode.SUCCESS,
        message: dioData.statusMessage,
        data: dioData.data,
      );
    }
    return DioResultModel.create(
      code: HttpResponseCode.UNKNOWN_ERROR,
      message: R.label.unknown,
      data: null,
    );
  }
}
