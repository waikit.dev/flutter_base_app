import 'package:flutter/material.dart';
import 'package:flutter_device_details/flutter_device_details.dart';
import 'package:flutter_base_app/constants.dart';
import 'package:get/get.dart';

abstract class Device {
  Device();

  Future<PackageInfo> _packageInfo = PackageInfo.fromPlatform();

  Future<String> get versionName async => (await _packageInfo).version;

  Future<String> get buildNumber async => (await _packageInfo).buildNumber;

  Future<String?>? model;

  Future<String?>? osVersion;

  Future<String?>? manufacturer;

  Future<String?>? identifier;

  Future<String?>? os;

  Locale? get deviceLocale => Get.deviceLocale;
}

class AndroidDevice extends Device {
  AndroidDevice() {
    _deviceInfo = _deviceInfoPlugin.androidInfo;
  }

  DeviceInfoPlugin _deviceInfoPlugin = DeviceInfoPlugin();

  Future<AndroidDeviceInfo?>? _deviceInfo;

  @override
  Future<String?>? get model async => (await _deviceInfo)?.model;

  @override
  Future<String?>? get osVersion async => (await _deviceInfo)?.version?.release;

  @override
  Future<String?>? get manufacturer async => (await _deviceInfo)?.manufacturer;

  @override
  Future<String?>? get identifier async => (await _deviceInfo)?.androidId;

  @override
  Future<String?>? get os async => Constants.android;
}

class IOSDevice extends Device {
  IOSDevice() {
    _deviceInfo = _deviceInfoPlugin.iosInfo;
  }

  DeviceInfoPlugin _deviceInfoPlugin = DeviceInfoPlugin();

  Future<IosDeviceInfo?>? _deviceInfo;

  @override
  Future<String?>? get model async => (await _deviceInfo)?.model;

  @override
  Future<String?>? get osVersion async => (await _deviceInfo)?.systemVersion;

  @override
  Future<String?>? get manufacturer async => Constants.apple;

  @override
  Future<String?>? get identifier async => (await _deviceInfo)?.identifierForVendor;

  @override
  Future<String?>? get os async => Constants.ios;
}

class WebDevice extends Device {}
