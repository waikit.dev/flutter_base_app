import 'package:get/get.dart';

class Strings {
  Strings();

  bool isNullOrEmpty(String? text) {
    return text == null || (text.isBlank == true);
  }

  bool isLength(String? text, int? length) {
    return (text?.length ?? 0) == length;
  }
}
