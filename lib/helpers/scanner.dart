import 'package:flutter/material.dart';
import 'package:flutter_base_app/app/application.dart';
import 'package:flutter_base_app/app/routers.dart';
import 'package:flutter_base_app/app/toolkit.dart';
import 'package:flutter_base_app/ui/widget/button_widget.dart';
import 'package:flutter_base_app/ui/widget/round_button_widget.dart';
import 'package:flutter_base_app/ui/widget/text_widget.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:qr_code_tools/qr_code_tools.dart';

class Scanner {
  Scanner();

  void scanQr({required Function(String?) onScanned}) {
    Toolkit.dialog.showBottomSheet(
      child: QrScannerPicker(
        onScanned: onScanned,
      ),
      canDismiss: true,
      backgroundColor: R.color.colorTransparent,
    );
  }
}

class QrScannerPicker extends StatelessWidget {
  QrScannerPicker({
    required this.onScanned,
  });

  final Function(String?) onScanned;

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(12.0),
          topRight: Radius.circular(12.0),
        ),
        color: R.color.colorDialogBg,
      ),
      padding: EdgeInsets.all(16.0),
      child: Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              TextWidget(
                text: R.label.scan_qr_from,
                textStyle: R.textTheme.headline5?.copyWith(fontSize: 20.0),
              ),
              RoundButtonWidget(
                themeColor: R.color.colorTransparent,
                size: 36.0,
                onTap: () {
                  Toolkit.navigator.goBack();
                },
                child: Icon(
                  Icons.close,
                  color: R.color.colorText,
                ),
              )
            ],
          ),
          SizedBox(
            height: 16.0,
          ),
          Row(
            children: [
              Expanded(
                child: OptionWidget(
                  icon: Icons.camera,
                  label: R.label.camera,
                  onTap: () {
                    startCameraScanner();
                  },
                ),
              ),
              SizedBox(
                width: 16.0,
              ),
              Expanded(
                child: OptionWidget(
                  icon: Icons.photo,
                  label: R.label.photo,
                  onTap: () {
                    startPhotoScanner();
                  },
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }

  void startCameraScanner() {
    Toolkit.permissions.requestPermission(Permission.camera, onGrant: () async {
      await Routers.launchQrScanner(onScanned: onScanned);
      Toolkit.navigator.goBack();
    }, onDeny: () {
      Toolkit.permissions.openSetting();
    }, onPermanentDeny: () {
      Toolkit.permissions.openSetting();
    });
  }

  void startPhotoScanner() {
    Permission permission = Toolkit.platform.isAndroid ? Permission.storage : Permission.photos;
    Toolkit.permissions.requestPermission(permission, onGrant: () async {
      await Toolkit.image.chooseFromGallery(onGetImage: (file) async {
        String data = await QrCodeToolsPlugin.decodeFrom(file.path);
        onScanned(data);
      });
      Toolkit.navigator.goBack();
    }, onDeny: () {
      Toolkit.permissions.openSetting();
    }, onPermanentDeny: () {
      Toolkit.permissions.openSetting();
    });
  }
}

class OptionWidget extends StatelessWidget {
  OptionWidget({
    required this.icon,
    required this.label,
    required this.onTap,
  });

  final IconData icon;

  final String label;

  final Function onTap;

  @override
  Widget build(BuildContext context) {
    return ButtonWidget(
      isBorderStyle: true,
      isWrap: true,
      onTap: onTap,
      themeColor: R.color.colorPrimary,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Icon(
            icon,
            color: R.color.colorPrimary,
            size: 48.0,
          ),
          SizedBox(
            height: 6.0,
          ),
          TextWidget(
            text: label,
            textStyle: R.textTheme.headline3,
          )
        ],
      ),
    );
  }
}
