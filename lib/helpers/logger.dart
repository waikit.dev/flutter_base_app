import 'package:flutter/foundation.dart';

class Logger {
  Logger();

  bool _logEnable = true;

  bool get logEnable => _logEnable;

  bool get isDebug => kDebugMode;

  void enable(bool enable) {
    _logEnable = enable;
  }

  void error({String key = 'Error', dynamic obj}) {
    _print(key, obj);
  }

  void info({String key = 'Info', dynamic obj}) {
    _print(key, obj);
  }

  void exception({String key = 'Exception', dynamic obj}) {
    _print(key, obj);
  }

  String message({String debug = '', String release = ''}) {
    return isDebug ? debug : release;
  }

  void _print(String key, dynamic obj) {
    if (!logEnable) return;
    print('=== $key ===');
    print(obj);
  }
}
