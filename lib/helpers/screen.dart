import 'package:flutter/material.dart';
import 'package:flutter_base_app/app/toolkit.dart';
import 'package:get/get.dart';

class Screen {
  Screen();

  double get screenWidth => Get.width;

  double get screenHeight => Get.height;

  double get appBarHeight => 56.0.scale();

  double get statusBarHeight => Get.mediaQuery.padding.top;

  void unFocus(BuildContext context) {
    FocusScope.of(context).unfocus();
  }

  double screenHeightWithoutAppBar() {
    return screenHeight - appBarHeight - statusBarHeight;
  }
}

extension DoubleExtension on double {
  double scale() {
    double totalWidthInXD = 375.0;
    double totalDeviceWidth = Toolkit.screen.screenWidth;
    double scaledWidth = totalDeviceWidth * (this / totalWidthInXD);
    return scaledWidth;
  }
}
