import 'dart:ui';

import 'package:flutter/foundation.dart';
import 'package:flutter_base_app/app/toolkit.dart';

class Colors {
  Colors();

  Color fromHex({@required String? hexStr, Color? defColor}) {
    StringBuffer buffer = StringBuffer();
    if (Toolkit.strings.isLength(hexStr, 6) || Toolkit.strings.isLength(hexStr, 7)) buffer.write('FF');
    buffer.write(hexStr?.replaceFirst('#', ''));
    int? colorHex = int.tryParse(buffer.toString(), radix: 16);
    return colorHex == null ? (defColor ?? Color(0xff000000)) : Color(colorHex);
  }
}
