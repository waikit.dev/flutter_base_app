import 'dart:io';

import 'package:image_picker/image_picker.dart';

class Image {
  Image();

  Future _pickImage({required ImageSource source, int imageQuality = 50, Function(File)? onGetImage}) async {
    PickedFile? file = await ImagePicker().getImage(
      source: source,
      imageQuality: imageQuality,
    );
    if (file != null) {
      onGetImage?.call(File(file.path));
    }
  }

  Future chooseFromGallery({int imageQuality = 50, Function(File)? onGetImage}) async {
    await _pickImage(
      source: ImageSource.gallery,
      imageQuality: imageQuality,
      onGetImage: onGetImage,
    );
  }

  Future takeFromCamera({int imageQuality = 50, Function(File)? onGetImage}) async {
    await _pickImage(
      source: ImageSource.camera,
      imageQuality: imageQuality,
      onGetImage: onGetImage,
    );
  }
}
