import 'package:flutter_base_app/app/application.dart';
import 'package:flutter_base_app/controller/controller.dart';
import 'package:flutter_base_app/net/entity/base_entity.dart';

enum ViewState {
  idle,
  busy,
  error,
  empty,
  unauthorised,
}

class ViewStateData {
  ViewStateData({
    required this.state,
    this.message,
    this.onAction,
  });

  final String? message;

  final ViewState state;

  final Function? onAction;
}

abstract class StateController extends Controller {
  StateController();

  ViewStateData _viewStateData = ViewStateData(state: ViewState.idle);

  ViewStateData get viewStateData => _viewStateData;

  Future initData({bool init = true});

  bool isEmptyData(BaseEntity entity) {
    return entity.data == null && entity.list == null;
  }

  bool checkEntityAndSetState(
    BaseEntity entity, {
    Function? onBadRequest,
    bool stopLoading = true,
    bool Function()? isEmpty,
  }) {
    if (stopLoading) hideLoader();

    if (entity.isSuccess) {
      if (isEmpty == null) {
        isEmpty = () => isEmptyData(entity);
      }

      if (isEmpty()) {
        setEmpty();
      } else {
        setIdle();
      }

      return true;
    }

    hideLoader();

    if (entity.isBadRequest) {
      setError(entity.message);
      onBadRequest?.call();
      return false;
    }

    if (entity.isUnauthorised) {
      setUnauthorized();
      return false;
    }

    setError(entity.dioMessage);
    return false;
  }

  void setBusy(bool busy) {
    _viewStateData = ViewStateData(state: busy ? ViewState.busy : ViewState.idle);
    update();
  }

  void setIdle() {
    _viewStateData = ViewStateData(state: ViewState.idle);
    update();
  }

  void setError(String? message) {
    _viewStateData = ViewStateData(state: ViewState.error, message: message, onAction: initData);
    update();
  }

  void setEmpty() {
    _viewStateData = ViewStateData(state: ViewState.empty, onAction: initData);
    update();
  }

  void setUnauthorized() {
    _viewStateData = ViewStateData(state: ViewState.unauthorised, message: R.label.unauthorised);
    update();
  }
}
