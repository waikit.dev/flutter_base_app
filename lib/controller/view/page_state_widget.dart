import 'package:flutter/material.dart';
import 'package:flutter_base_app/app/application.dart';
import 'package:flutter_base_app/controller/state_controller.dart';
import 'package:flutter_base_app/ui/builder/general_view_builder.dart';

class PageStateWidget extends StatelessWidget {
  PageStateWidget({
    required this.child,
    required this.viewState,
    this.isSliver = false,
    this.sliverBgColor,
  });

  final Widget child;

  final bool isSliver;

  final Color? sliverBgColor;

  final ViewStateData viewState;

  @override
  Widget build(BuildContext context) {
    Widget content;
    switch (viewState.state) {
      case ViewState.busy:
        content = getBusyView();
        break;
      case ViewState.error:
        content = getErrorView();
        break;
      case ViewState.empty:
        content = getEmptyView();
        break;
      case ViewState.unauthorised:
        content = getUnauthorisedView();
        break;
      case ViewState.idle:
        return child;
      default:
        content = Container();
    }

    if (isSliver) {
      return SliverFillViewport(
        delegate: SliverChildBuilderDelegate(
          (context, index) => Container(
            child: content,
            color: sliverBgColor ?? R.color.colorScaffoldBg,
          ),
          childCount: 1,
        ),
      );
    } else {
      return content;
    }
  }

  Widget getBusyView() {
    return GeneralViewBuilder.busyStateWidget;
  }

  Widget getErrorView() {
    return GeneralViewBuilder.errorStateWidget(
      title: viewState.message,
      action: R.label.click_here_to_retry,
      onAction: viewState.onAction,
    );
  }

  Widget getEmptyView() {
    return GeneralViewBuilder.emptyStateWidget(
      title: R.label.no_data,
      action: R.label.click_here_to_retry,
      onAction: viewState.onAction,
    );
  }

  Widget getUnauthorisedView() {
    return GeneralViewBuilder.unAuthStateWidget(
      title: R.label.unauthorised,
      action: R.label.click_here_to_login_again,
      onAction: viewState.onAction,
    );
  }
}
