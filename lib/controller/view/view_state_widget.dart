import 'package:flutter/material.dart';
import 'package:flutter_base_app/app/application.dart';
import 'package:flutter_base_app/ui/widget/text_widget.dart';
import 'package:flutter_base_app/helpers/screen.dart';

typedef ViewStateWidgetBuilder = Widget Function({
  String? title,
  String? action,
  Function? onAction,
});

class ViewStateWidget extends StatelessWidget {
  const ViewStateWidget({
    this.title,
    this.action,
    this.onAction,
  });

  final String? title;

  final String? action;

  final Function? onAction;

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          TextWidget(
            text: title ?? '',
            textStyle: R.textTheme.headline5?.copyWith(color: R.color.colorText, fontSize: 18.0.scale()),
            textAlign: TextAlign.center,
          ),
          SizedBox(
            height: 12.0,
          ),
          InkWell(
            onTap: () {
              onAction?.call();
            },
            child: TextWidget(
              text: action ?? '',
              textStyle: R.textTheme.headline2?.copyWith(color: R.color.colorLink),
            ),
          )
        ],
      ),
    );
  }
}
