import 'package:flutter_base_app/controller/state_controller.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:flutter_base_app/net/entity/base_entity.dart';

abstract class StateListController<T> extends StateController {
  StateListController();

  final RefreshController refreshController = RefreshController();

  int _page = 1;

  int get page => _page;

  List<T> _list = [];

  List<T> get list => _list;

  int get listCount => list.length;

  T itemAtIndex(int index) => list.elementAt(index);

  Future initData({bool init = true}) async {
    setBusy(init);
    await onRefresh(init: true);
  }

  Future onRefresh({bool init = false}) async {
    _page = 1;
    var list = await loadData(page: page);
    refreshController.resetNoData();
    refreshController.refreshCompleted(resetFooterState: true);

    setList(list);
  }

  Future nextPage() async {
    try {
      _page++;
      var list = await loadData(page: page);
      if (list.length == 0) {
        refreshController.loadNoData();
        return;
      }
      addAll(list);
      refreshController.loadComplete();
    } catch (e) {
      _page--;
      refreshController.loadFailed();
    }
  }

  void addAll(List<T> list) {
    if (list.length == 0) return;
    _list.addAll(list);
  }

  void setList(List<T> list) => _list = list;

  Future<List<T>> loadData({int page});

  @override
  bool checkEntityAndSetState(
    BaseEntity entity, {
    Function? onBadRequest,
    bool stopLoading = true,
    bool Function()? isEmpty,
  }) {
    return super.checkEntityAndSetState(
      entity,
      stopLoading: stopLoading,
      onBadRequest: onBadRequest,
      isEmpty: isEmpty ?? () => isEmptyList(entity.list) && page == 1,
    );
  }

  bool isEmptyList(List? list) {
    return list == null || list.length == 0;
  }
}
