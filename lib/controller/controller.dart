import 'package:flutter_base_app/app/controllers.dart';
import 'package:flutter_base_app/app/controller/app_controller.dart';
import 'package:flutter_base_app/app/toolkit.dart';
import 'package:flutter_base_app/ui/builder/general_view_builder.dart';
import 'package:get/get.dart';
import 'package:flutter_base_app/net/entity/base_entity.dart';

abstract class Controller extends GetxController {
  Controller();

  String get tag => this.toString();

  @override
  void onInit() {
    super.onInit();
    if (registerOnReset) {
      Controllers.get<AppController>()?.registerResetController(tag, this);
    }
  }

  void showLoader() async {
    Toolkit.loader.showLoading();
  }

  void hideLoader() {
    Toolkit.loader.hideLoading();
  }

  Future showErrorDialog({
    required String message,
    String? title,
    String? action,
    Function? onAction,
    bool canDismiss = true,
  }) async {
    await Toolkit.dialog.showDialog(
      child: GeneralViewBuilder.errorDialog(
        title: title,
        message: message,
        action: action,
        onAction: onAction,
      ),
      canDismiss: canDismiss,
    );
  }

  Future showSuccessDialog({
    required String message,
    String? title,
    String? action,
    Function? onAction,
    bool canDismiss = true,
  }) async {
    await Toolkit.dialog.showDialog(
      child: GeneralViewBuilder.successDialog(
        title: title,
        message: message,
        action: action,
        onAction: onAction,
      ),
      canDismiss: canDismiss,
    );
  }

  bool checkEntity(
    BaseEntity entity, {
    bool stopLoading = true,
    bool showError = true,
    Function? onBadRequest,
    Function? onSuccess,
    Function? onDioError,
  }) {
    if (stopLoading) hideLoader();

    if (entity.isSuccess) {
      onSuccess?.call();
      return true;
    }

    hideLoader();

    if (entity.isBadRequest && onBadRequest != null) {
      onBadRequest.call();
      return false;
    }

    if (entity.isBadRequest) {
      showErrorDialog(message: entity.message ?? '');
      return false;
    }

    if (onDioError != null) {
      onDioError.call();
      return false;
    }

    showErrorDialog(message: entity.dioMessage ?? '');
    return false;
  }

  bool get registerOnReset => false;

  Future onReset() async {}
}
