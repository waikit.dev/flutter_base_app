import 'package:flutter_base_app/app/toolkit.dart';

const ACCESS_TOKEN = 'access_token';

abstract class BaseStorage {
  void saveAccessToken(String token) async {
    await Toolkit.secureStorage.saveString(ACCESS_TOKEN, token);
  }

  Future<String> readAccessToken() async {
    return await Toolkit.secureStorage.readString(ACCESS_TOKEN);
  }

  Future<bool> hasAccessToken() async {
    return !Toolkit.strings.isNullOrEmpty(await readAccessToken());
  }
}
